/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {

    ResourceInfo::ResourceInfo() {}

    ResourceInfo::ResourceInfo(const json &$data) {
        if (!$data.empty()) {
            if ($data.find("name") != $data.end()) {
                this->name = $data["name"];
            }
            if ($data.find("location") != $data.end()) {
                this->location = $data["location"];
            }
            if ($data.find("copyOnly") != $data.end()) {
                this->copyOnly = $data["copyOnly"];
            }
            if ($data.find("type") != $data.end()) {
                this->type = $data["type"];
            }
            if ($data.find("projectName") != $data.end()) {
                this->projectName = $data["projectName"];
            }
            if ($data.find("releaseName") != $data.end()) {
                this->releaseName = $data["releaseName"];
            }
            if ($data.find("platform") != $data.end()) {
                this->platform = $data["platform"];
            }
            if ($data.find("version") != $data.end()) {
                this->version = $data["version"];
            }
        }
    }

    const string &ResourceInfo::getName() const {
        return this->name;
    }

    void ResourceInfo::setName(const string &$name) {
        this->name = $name;
    }

    const string &ResourceInfo::getLocation() const {
        return this->location;
    }

    void ResourceInfo::setLocation(const string &$location) {
        this->location = $location;
    }

    bool ResourceInfo::isCopyOnly() const {
        return this->copyOnly;
    }

    void ResourceInfo::setCopyOnly(bool $copyOnly) {
        this->copyOnly = $copyOnly;
    }

    const string &ResourceInfo::getType() const {
        return this->type;
    }

    void ResourceInfo::setType(const string &$type) {
        this->type = $type;
    }

    const string &ResourceInfo::getProjectName() const {
        return this->projectName;
    }

    void ResourceInfo::setProjectName(const string &$projectName) {
        this->projectName = $projectName;
    }

    const string &ResourceInfo::getReleaseName() const {
        return this->releaseName;
    }

    void ResourceInfo::setReleaseName(const string &$releaseName) {
        this->releaseName = $releaseName;
    }

    const string &ResourceInfo::getPlatform() const {
        return this->platform;
    }

    void ResourceInfo::setPlatform(const string &$platform) {
        this->platform = $platform;
    }

    const string &ResourceInfo::getVersion() const {
        return this->version;
    }

    void ResourceInfo::setVersion(const string &$version) {
        this->version = $version;
    }

    string ResourceInfo::ToString() const {
        json data = {
                {"name",        this->name},
                {"location",    this->location},
                {"copyOnly",    this->copyOnly},
                {"type",        this->type},
                {"projectName", this->projectName},
                {"releaseName", this->releaseName},
                {"platform",    this->platform},
                {"version",     this->version}
        };

        return data.dump();
    }

    std::ostream &operator<<(std::ostream &$os, const ResourceInfo &$info) {
        $os << $info.ToString();
        return $os;
    }
}
