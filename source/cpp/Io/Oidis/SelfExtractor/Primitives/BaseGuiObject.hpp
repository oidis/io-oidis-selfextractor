/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_PRIMITIVES_BASEGUIOBJECT_HPP_
#define IO_OIDIS_SELFEXTRACTOR_PRIMITIVES_BASEGUIOBJECT_HPP_

namespace Io::Oidis::SelfExtractor::Primitives {
    /**
     * This class defines base class for each user controls classes.
     */
    class BaseGuiObject {
     public:
        /**
         * Construct default GUI object.
         */
        BaseGuiObject();

        /**
         * Constructs GUI object from JSON configuration.
         * @param $config Specify configuration to be used as object data.
         */
        explicit BaseGuiObject(const json &$config);

        /**
         * @return Returns size of user control.
         */
        const Structures::Size &getSize() const;

        /**
         * @param $size Specify new size for user control.
         */
        void setSize(const Structures::Size &$size);

        /**
         * @return Returns position of user control.
         */
        const Structures::Position &getPosition() const;

        /**
         * @param $position Specify new position of user control.
         */
        void setPosition(const Structures::Position &$position);

        /**
         * @return Returns visibility state of user control.
         */
        bool isVisible() const;

        /**
         * @param $visible Sets visibility state for user control.
         */
        void setVisible(bool $visible);

        /**
         * Implement this method in every single derived class.
         * @param $graphics Specify GDI graphics which will contains this user control.
         */
#ifdef WIN_PLATFORM

        virtual void Draw(Gdiplus::Graphics &$graphics) = 0;

#endif

     private:
        Structures::Size size;
        Structures::Position position;
        bool visible = true;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_PRIMITIVES_BASEGUIOBJECT_HPP_
