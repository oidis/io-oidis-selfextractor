/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

Process = function ($cwd, $args, $done) {
    if (filesystem.Exists($cwd + "/release/lib/libelf.a")) {
        LogIt.Info("Libelf library already exists. Install script skipped.");
        $done();
    } else {
        if (!EnvironmentHelper.IsWindows()) {
            terminal.Spawn("./configure", ["--prefix=" + $cwd + "/release"], $cwd, ($exitCode) => {
                if ($exitCode !== 0) {
                    LogIt.Error("Cannot autogen libffi.");
                } else {
                    terminal.Spawn("make", ["-j" + EnvironmentHelper.getCores()], $cwd, ($exitCode) => {
                        if ($exitCode !== 0) {
                            LogIt.Error("Cannot compile libelf.");
                        }
                        terminal.Spawn("make", ["install"], $cwd, ($exitCode) => {
                            if ($exitCode !== 0) {
                                LogIt.Error("Cannot install libelf.");
                            }
                            $done();
                        });
                    });
                }
            });
        } else {
            LogIt.Info("Libelf is not used on Windows.");
            $done();
        }
    }
};
