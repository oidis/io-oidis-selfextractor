/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {

    Font::Font() {
#ifdef WIN_PLATFORM
        this->font = nullptr;
#endif
        this->size = 14;
        this->bold = false;
    }

    Font::Font(const string &$name, int $size, bool $bold)
            : name($name),
              size($size),
              bold($bold) {
#ifdef WIN_PLATFORM
        this->font = nullptr;
#endif
    }

    Font::Font(const json &$config) {
        if (!$config.empty() && $config.is_object()) {
            this->name = $config.value("name", this->name);
            this->size = $config.value("size", this->size);
            this->bold = $config.value("bold", this->bold);
            this->color = Color($config.value("color", this->color.ToString()));
        }
    }

    const string &Font::getName() const {
        return this->name;
    }

    void Font::setName(const string &$name) {
        this->name = $name;
    }

    int Font::getSize() const {
        return this->size;
    }

    void Font::setSize(int $size) {
        this->size = $size;
    }

    bool Font::isBold() const {
        return this->bold;
    }

    void Font::setBold(bool $bold) {
        this->bold = $bold;
    }

#ifdef WIN_PLATFORM

    Gdiplus::Font *Font::getFont() const {
        Font *ptr = const_cast<Font *>(this);
        ptr->font = new Gdiplus::Font(std::wstring(this->name.begin(), this->name.end()).c_str(), this->size,
                                      this->bold ? Gdiplus::FontStyle::FontStyleBold : Gdiplus::FontStyle::FontStyleRegular);
        return this->font;
    }

#endif

    const Color &Font::getColor() const {
        return this->color;
    }

    void Font::setColor(const Color &$color) {
        this->color = $color;
    }
}
