/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_COLOR_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_COLOR_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * This class defines object to store RGBA color model.
     */
    class Color {
     public:
        /**
         * Construct default color (transparent).
         */
        Color();

        /**
         * Constructs color from color string. Check FromString for supported formats.
         * @param $value
         */
        explicit Color(const string &$value);

        /**
         * Constructs color from values. Alpha channel is 255 by default.
         * @param $red Specify red channel 0-255.
         * @param $green Specify green channel 0-255.
         * @param $blue Specify blue channel 0-255.
         */
        Color(int $red, int $green, int $blue);

        /**
         * Constructs color from values.
         * @param $red Specify red channel 0-255.
         * @param $green Specify green channel 0-255.
         * @param $blue Specify blue channel 0-255.
         * @param $alpha Specify alpha channel 0-255.
         */
        Color(int $red, int $green, int $blue, int $alpha);

        /**
         * Initialize color from color string.
         * Supported formats:
         *  - #(AA)RRGGBB
         *  - 0x(AA)RRGGBB
         *  - (AA)RRGGBB
         *  .
         *  Each values are in two-digits hexadecimal. Alpha channel is optional, if not explicitly defined 0xff is used by default.
         * @param $value Specify color string.
         */
        void FromString(const string &$value);

        /**
         * Converts color to color string.
         * @param $prefix Specify color string prefix.
         * @return Returns prefixed color string with alpha channel (AARRGGBB).
         */
        string ToString(const string &$prefix = "#") const;

        /**
         * @return Returns GDI color object constructed from this object.
         */
#ifdef WIN_PLATFORM

        Gdiplus::Color getColor() const;

#endif

     private:
#ifdef WIN_PLATFORM
        Gdiplus::Color color = Gdiplus::Color::Transparent;
#endif
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_COLOR_HPP_
