/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::ResponseApi::Handlers {

    void StatusResponse::OnChange(const json &$object) const {
        if (this->onChange != nullptr) {
            json data = {{"type", "onchange"},
                         {"data", $object}};
            this->onChange(data);
        }
    }

    void StatusResponse::OnComplete(const string &$data) const {
        if (this->onComplete != nullptr) {
            json data = {{"type", "oncomplete"},
                         {"data", $data}};
            this->onComplete(data);
        }
    }

    void StatusResponse::OnComplete(const json &$object, const string &$data) const {
        if (this->onCompleteB != nullptr) {
            this->onCompleteB($object, $data);
        }
    }

    void StatusResponse::setOnChange(const function<void(const json &)> &$onChange) {
        this->onChange = $onChange;
    }

    void StatusResponse::setOnComplete(const function<void(const string &)> &$onComplete) {
        this->onComplete = $onComplete;
    }

    void StatusResponse::setOnComplete(const function<void(const json &, const string &)> &$onComplete) {
        this->onCompleteB = $onComplete;
    }
}
