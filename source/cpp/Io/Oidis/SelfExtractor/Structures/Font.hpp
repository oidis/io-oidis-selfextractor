/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_FONT_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_FONT_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * This class defines font.
     */
    class Font {
     public:
        /**
         * Constructs default font.
         */
        Font();

        /**
         * Constructs font from properties.
         * @param $name Specify font name. Must exists on current machine.
         * @param $size Specify font size.
         * @param $bold Specify true to use bold, false otherwise.
         */
        explicit Font(const string &$name, int $size = 11, bool $bold = false);

        /**
         * Constructs font from JSON configuration.
         * @param $config Specify configuration data
         */
        explicit Font(const json &$config);

        /**
         * @return Returns font name.
         */
        const string &getName() const;

        /**
         * @param $name Specify font name.
         */
        void setName(const string &$name);

        /**
         * @return Returns font size.
         */
        int getSize() const;

        /**
         * @param $size Specify font size.
         */
        void setSize(int $size);

        /**
         * @return Returns true for bold font, false otherwise.
         */
        bool isBold() const;

        /**
         * @param $bold Specify true to use bold font of false for regular.
         */
        void setBold(bool $bold);

        /**
         * @return Returns font color.
         */
        const Color &getColor() const;

        /**
         * @param $color Specify font color.
         */
        void setColor(const Color &$color);

        /**
         * @return Returns GDI font object constructed from this font.
         */
#ifdef WIN_PLATFORM

        Gdiplus::Font *getFont() const;

#endif

     private:
        string name;
        int size;
        bool bold;
#ifdef WIN_PLATFORM
        Gdiplus::Font *font;
#endif
        Io::Oidis::SelfExtractor::Structures::Color color{"#ff000000"};
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_FONT_HPP_
