/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Utils {
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::SelfExtractor::Structures::CacheEntry;

    CacheManager &CacheManager::getInstance() {
        static CacheManager instance;
        return instance;
    }

    void CacheManager::Init(const string &$filePath) {
        if (!this->isDisabled()) {
            this->cacheFilePath = $filePath;
            string data = FileSystem::Read($filePath);
            if (!data.empty()) {
                json objCache = json::parse(data);
                for (json::iterator it = objCache.begin(); it != objCache.end(); ++it) {
                    this->cache.push_back(CacheEntry(*it));
                }
            }
        }
    }

    void CacheManager::Save() {
        if (!this->isDisabled()) {
            json data = json::array();
            for (auto it = this->cache.begin(); it != this->cache.end(); ++it) {
                data.push_back(it->ToJson());
            }
            FileSystem::Write(this->cacheFilePath, data.dump(2));
        }
    }

    std::vector<CacheEntry> &CacheManager::getCache() const {
        return this->cache;
    }

    const string &CacheManager::getCacheFilePath() const {
        return this->cacheFilePath;
    }

    void CacheManager::setCacheFilePath(const string &$cacheFilePath) {
        this->cacheFilePath = $cacheFilePath;
    }

    CacheEntry CacheManager::getEntry(const string &$key) {
        CacheEntry retVal;
        auto it = std::find_if(this->cache.begin(), this->cache.end(), [&](const CacheEntry &$entry) -> bool {
            return boost::iequals($entry.getKey(), $key);
        });

        if (it != this->cache.end()) {
            retVal = *it;
        }

        return retVal;
    }

    void CacheManager::Emplace(const CacheEntry &$entry) {
        if (!this->isDisabled()) {
            if (this->getEntry($entry.getKey()).Empty()) {
                this->cache.push_back($entry);
            } else {
                std::for_each(this->cache.begin(), this->cache.end(), [&](CacheEntry &$item) {
                    if (boost::iequals($item.getKey(), $entry.getKey())) {
                        $item = $entry;
                    }
                });
            }
        }
    }

    bool CacheManager::isDisabled() const {
        return this->disabled;
    }

    void CacheManager::setDisabled(bool $disabled) {
        this->disabled = $disabled;
    }
}
