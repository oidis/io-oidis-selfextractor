/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_
#define IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_

namespace Io::Oidis::SelfExtractor::Utils {
    /**
     * ResourceManager static class provides API to work with embedded resources.
     */
    class ResourcesManager : private Io::Oidis::XCppCommons::Interfaces::INonCopyable,
                             private Io::Oidis::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Find configuration embedded in group "CONFIG".
         * @param $name Specify configuration name
         * @return Returns configuration or empty JSON object if not find.
         */
        static json FindConfig(const string &$name);

        /**
         * Read specified config.
         * @param $name Specify config name.
         * @return Returns config in raw string.
         */
        static string ReadConfig(const string &$name);

        /**
         * Read specified resource.
         * @param $name Specify resource name.
         * @param $handler Callback to be called for red data.
         */
        static void ReadResource(const string &$name, std::function<void(const char *, unsigned int)> const &$handler);

        static void setModulePath(const string &$path);

     protected:
        static void *getModule();
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_
