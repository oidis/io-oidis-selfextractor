/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SIZE_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SIZE_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * Size class stores vertical and horizontal size values.
     */
    class Size {
     public:
        /**
         * Constructs default size object.
         */
        Size();

        /**
         * Constructs size from values.
         * @param $width Specify horizontal size.
         * @param $height Specify vertical size.
         */
        Size(int $width, int $height);

        /**
         * @return Returns horizontal size.
         */
        int getWidth() const;

        /**
         * @param $width Specify horizontal size.
         */
        void setWidth(int $width);

        /**
         * @return Returns vertical size.
         */
        int getHeight() const;

        /**
         * @param $height Specify vertical size.
         */
        void setHeight(int $height);

        bool operator<(const Size &$rhs) const;

        bool operator>(const Size &$rhs) const;

        bool operator<=(const Size &$rhs) const;

        bool operator>=(const Size &$rhs) const;

        bool operator==(const Size &$rhs) const;

        bool operator!=(const Size &$rhs) const;

        friend std::ostream &operator<<(std::ostream &$os, const Size &$size);

     private:
        int width = 0;
        int height = 0;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SIZE_HPP_
