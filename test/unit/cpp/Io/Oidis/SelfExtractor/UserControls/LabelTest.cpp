/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
    TEST(LabelTest, Construction) {
        json data = {
                {"text",    "Some test string"},
                {"x",       111},
                {"y",       123},
                {"width",   200},
                {"height",  35},
                {"justify", "LEFT"},
                {"visible", true},
                {"font",
                            {
                                    {"name", "Courier New"},
                                    {"size", 14},
                                    {"bold", false},
                                    {"color", "#ff0000"}
                            }
                }
        };

        Label label(data);
        ASSERT_STREQ("Some test string", label.getText().c_str());
        ASSERT_EQ(111, label.getPosition().getX());
        ASSERT_EQ(123, label.getPosition().getY());
        ASSERT_EQ(200, label.getSize().getWidth());
        ASSERT_EQ(35, label.getSize().getHeight());
        ASSERT_EQ(Enums::TextJustifyType::LEFT, label.getJustifyType());
        ASSERT_STREQ("Courier New", label.getFont().getName().c_str());
        ASSERT_EQ(14, label.getFont().getSize());
        ASSERT_FALSE(label.getFont().isBold());
        ASSERT_STREQ(Structures::Color(0xff, 0, 0).ToString().c_str(), label.getFont().getColor().ToString().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::UserControls
