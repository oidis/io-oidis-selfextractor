/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019-2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

#ifdef WIN_PLATFORM

#include <shellapi.h>  // NOLINT
#include <dshow.h>

#endif

namespace Io::Oidis::SelfExtractor {
    using Io::Oidis::SelfExtractor::Structures::SelfExtractorArgs;
    using Io::Oidis::SelfExtractor::Utils::ResourcesManager;
    using Io::Oidis::SelfExtractor::Structures::ResourceInfo;
    using Io::Oidis::SelfExtractor::Utils::Localization;
    using Io::Oidis::XCppCommons::Utils::ArgsParser;
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::XCppCommons::Events::ThreadPool;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::System::IO::Path;
    using Io::Oidis::XCppCommons::System::Net::Url;
    using Io::Oidis::XCppCommons::System::Process::Child;
    using Io::Oidis::XCppCommons::System::Process::ExecuteOptions;
    using Io::Oidis::XCppCommons::System::Process::SpawnOptions;
    using Io::Oidis::XCppCommons::System::Process::Terminal;
    using Io::Oidis::XCppCommons::EnvironmentArgs;
    using Io::Oidis::XCppCommons::Primitives::String;
    using Io::Oidis::XCppCommons::Primitives::ArrayList;
    using Io::Oidis::XCppCommons::Utils::ObjectEncoder;
    using Io::Oidis::XCppCommons::Utils::ObjectDecoder;
    using Io::Oidis::XCppCommons::Utils::Convert;
    using Io::Oidis::XCppCommons::Interfaces::IResponse;
    using Io::Oidis::XCppCommons::System::ResponseApi::ResponseFactory;
    using Io::Oidis::XCppCommons::System::ResponseApi::Handlers::CallbackResponse;
    using Io::Oidis::SelfExtractor::Structures::CacheEntry;
    using Io::Oidis::SelfExtractor::Utils::CacheManager;
    using Io::Oidis::SelfExtractor::Utils::ResourcesUpdater;
    using Io::Oidis::SelfExtractor::Utils::JSON;
    using Io::Oidis::SelfExtractor::ResponseApi::Handlers::StatusResponse;

    static string appDataPath;

    Application::Application() {
        LogIt::setOnPrint([](const string &$msg) {
            std::cout << $msg;
        });

        Application::setAppDataPath(FileSystem::getLocalAppDataPath() + "/WUIFramework");
    }

    int Application::Run(const int $argc, const char **$argv) {
        SelfExtractorArgs args;

        int parseStatus = ArgsParser::Parse(args, $argc, $argv);
        if (parseStatus == 0) {
            if (args.isDebug()) {
                LogIt::setLevel(Io::Oidis::XCppCommons::Enums::LogLevel::ALL);
            }

            if (!args.getExtModule().empty()) {
                ResourcesManager::setModulePath(args.getExtModule());
            }

            if (args.isPrintConfiguration()) {
                std::cout << "intern.conf.json:" << std::endl << ResourcesManager::FindConfig("INTERN_CONF_JSON").dump(2) << std::endl
                          << std::endl;
                std::cout << "selfextractor.conf.json:" << std::endl << ResourcesManager::FindConfig("SELFEXTRACTOR_CONF_JSON").dump(2)
                          << std::endl;
                return 0;
            }
            std::cout << "Starting selfextractor" << std::endl;
            if (!FileSystem::Exists(Application::getAppDataPath())) {
                FileSystem::CreateDirectory(Application::getAppDataPath());
            }

            CacheManager::getInstance().setDisabled(args.isDisableCache());
            CacheManager::getInstance().Init(Application::getAppDataPath() + "/cache.json");
            this->resourcesUpdater.reset(new ResourcesUpdater());

            if (!this->prepareIntern()) {
#ifdef WIN_PLATFORM
                return ERROR_INVALID_DATA;
#else
                return ENODATA;
#endif
            }

            json splashConfig = {};

            if (Application::prepareConfig(splashConfig) && !splashConfig.empty()) {
                if (!args.isHeadless()) {
                    auto resolveRelative = [&](const string &$path) -> string {
                        boost::filesystem::path path($path);
                        if (path.is_relative()) {
                            path = boost::filesystem::path(Application::getAppDataPath()) / path;
                        }
                        return path.normalize().make_preferred().string();
                    };
                    if (splashConfig.find("background") != splashConfig.end()) {
                        splashConfig["background"] = resolveRelative(
                                this->resourcesUpdater->DownloadResource(splashConfig["background"]));
                    }
                    if (splashConfig.find("userControls") != splashConfig.end() &&
                        splashConfig["userControls"].find("progress") != splashConfig["userControls"].end()) {
                        json progress = splashConfig["userControls"]["progress"];
                        if (progress.find("foreground") != progress.end()) {
                            splashConfig["userControls"]["progress"]["foreground"] = resolveRelative(
                                    this->resourcesUpdater->DownloadResource(
                                            splashConfig["userControls"]["progress"]["foreground"]));
                        }
                        if (progress.find("background") != progress.end()) {
                            splashConfig["userControls"]["progress"]["background"] = resolveRelative(
                                    this->resourcesUpdater->DownloadResource(
                                            splashConfig["userControls"]["progress"]["background"]));
                        }
                    }
                }

                LogIt::Info("Configuration json:\n{0}", splashConfig.dump());

                std::vector<ResourceInfo> resources;
                string executablePath;
                string installPath;
                std::vector<string> executableArgs;

#ifdef WIN_PLATFORM
                Gdiplus::GdiplusStartupInput gdiplusStartupInput;
                ULONG_PTR gdiPlusToken = 0;
                if (!args.isHeadless()) {
                    Gdiplus::GdiplusStartup(&gdiPlusToken, &gdiplusStartupInput, nullptr);  // initialize GDI+
                } else {
                    LogIt::Info("Starting in headless mode");
                }
#endif
                shared_ptr<boost::lockfree::spsc_queue<string>> queue(new boost::lockfree::spsc_queue<string>(100));

                ThreadPool threadPool;
                boost::barrier barrier(2);
                threadPool.AddThread("gui_thread", [&](...) {
#ifdef WIN_PLATFORM
                    MSG msg = {};
                    BOOL bRet;
                    LogIt::Info("Starting GUI thread.");

                    UserControls::SplashScreen splashScreen(splashConfig);
                    resources = splashScreen.getResources();
                    executablePath = splashScreen.getExecutable();
                    installPath = splashScreen.getInstallPath();
                    splashScreen.setCommandQueue(queue);
                    executableArgs = splashScreen.getExecutableArgs();

                    if (!args.isHeadless()) {
                        splashScreen.Create();
                    }
                    barrier.wait();  // wait app_thread for splashscreen created

                    if (!args.isHeadless()) {
                        while ((bRet = GetMessage(&msg, nullptr, 0, 0)) > 0) {
                            if (bRet == -1) {
                                break;
                            }

                            TranslateMessage(&msg);
                            DispatchMessage(&msg);
                        }

                        if (threadPool.IsRunning("app_thread")) {
                            Gdiplus::GdiplusShutdown(gdiPlusToken);  // finalize GDI+
                            LogIt::Warning("Application has been stopped. Possible unknown exception after is caused by this action.");
                            /*
                             * TODO(B58790) force finish should be replaced by better way, this will fail on unexpected exception
                             * because of undefined object state in app_thread. The problem is with termination of download/copy/unpack
                             * tasks which can not be finished correctly by another "easy" way at this time.
                             */
                            return;
                        }
                    }
                    LogIt::Info("Application GUI thread finished.");
#else
                    barrier.wait();
#endif
                });

                threadPool.AddThread("app_thread", [&](...) {
                    typedef boost::filesystem::path fspath;
                    auto removeExtension = [](string const &$file) -> string {
                        return String::Remove($file, fspath($file).extension().string());
                    };

                    string basePath = removeExtension(
                            (fspath(FileSystem::getTempPath()) / Application::ResolveOriginalFileName(
                                    EnvironmentArgs::getInstance().getExecutablePath() + "/" +
                                    EnvironmentArgs::getInstance().getExecutableName())).string());

                    barrier.wait();
                    LogIt::Info("Starting resource processing thread.");

                    if (!installPath.empty()) {
                        if (fspath(installPath).is_absolute()) {
                            basePath = installPath;
                            LogIt::Warning("SelfExtractor uses absolute path \"{0}\". Please avoid usage of absolute "
                                           "paths for other than debug purposes.", installPath);
                        } else {
                            basePath = Path::Expand(installPath);
                            LogIt::Info(R"(Expand path "{0}" > "{1}")", installPath, basePath);
                        }
                    } else {
                        LogIt::Info("Install path is empty: using default at %TEMP%/<application name>");
                    }

                    LogIt::Info("Prepare install path at \"{0}\".", basePath);

                    // do not delete basePath but still try to kill each processes (weak kill)
                    bool cleanupDisabled = splashConfig.value("cleanupDisabled", false) || args.isCleanupDisabled();
                    // if basePath exists and contains executable, than skip cleanup/copy and only execute binary
                    bool runOnly = splashConfig.value("runOnly", false) || args.isRunOnly();

                    if (runOnly) {
                        LogIt::Info("Run-only mode automatically suppress cleanup feature.");
                        cleanupDisabled = true;
                    }
                    bool status = true;
                    if (FileSystem::Exists(basePath)) {
                        if (!cleanupDisabled) {
                            status = FileSystem::Delete(basePath);
                        }

                        if (!status) {
                            std::vector<string> apps = FileSystem::Expand(basePath + "/**/*.exe");
                            LogIt::Debug("Applications found in base path \"{0}\":\n\t{1}", basePath, ArrayList::Join(apps, ";\n\t"));

                            using Io::Oidis::XCppCommons::System::Process::TaskManager;
                            for (auto &item : apps) {
                                string app = String::Replace(item, "\\", "/");
                                auto tasks = TaskManager::Find(
                                        String::Substring(app, static_cast<unsigned int>(String::IndexOf(app, "/", false) + 1)));
                                std::vector<string> acc;
                                for (auto &$task : tasks) {
                                    acc.emplace_back($task.getPath());
                                    if (app == String::Replace($task.getPath(), "\\", "/")) {
                                        TaskManager::Terminate($task);
                                    }
                                }
                                LogIt::Debug("Found tasks:\n\t{0}", ArrayList::Join(acc, ";\n\t"));
                            }

                            if (!cleanupDisabled) {
                                status = FileSystem::Delete(basePath);
                            }
                        }
                    } else {
                        runOnly = false;
                        LogIt::Info("Run only detected but install path not exists, "
                                    "so selfextractor will continue with default behaviour.");
                    }

                    if (status) {
                        FileSystem::CreateDirectory(basePath);

                        StatusResponse statusResponse;

                        int step = 0;
                        auto asyncProcess = [&](const json $data) {
                            if (!args.isHeadless()) {
                                switch (step) {
                                    case 1: {
                                        // download
                                        if (boost::iequals($data.value("type", ""), "onchange")) {
                                            int cur = $data["data"].value("currentValue", 0);
                                            int max = $data["data"].value("rangeEnd", 1);
                                            queue->push(String::Format(Localization::getInstance().getLocalized("downloading"),
                                                                       static_cast<int>(cur * 100.0 / max)));
                                            queue->push(String::Format("!progress:{0}/{1}", cur, max));
                                        } else if (boost::iequals($data.value("type", ""), "oncomplete")) {
                                            queue->consume_all([](auto a) {});
                                            queue->push(String::Format(Localization::getInstance().getLocalized("downloading"), 100));
                                            queue->push(String::Format("!progress:{0}/{1}", 1, 1));
                                        }
                                        break;
                                    }
                                    case 2: {
                                        // copy
                                        if (boost::iequals($data.value("type", ""), "onchange")) {
                                            int cur = $data["data"].value("currentValue", 0);
                                            int max = $data["data"].value("rangeEnd", 1);
                                            queue->push(String::Format(Localization::getInstance().getLocalized("copying"),
                                                                       static_cast<int>(cur * 100.0 / max)));
                                            queue->push(String::Format("!progress:{0}/{1}", cur, max));
                                        }
                                        break;
                                    }
                                    case 3: {
                                        // extract
                                        queue->push(String::Format(Localization::getInstance().getLocalized("extracting")));
                                        break;
                                    }
                                    default:
                                        break;
                                }
                            }
                        };
                        struct timeval tp = {};
                        auto calcMs = [&tp]() -> long {
                            gettimeofday(&tp, nullptr);
                            return tp.tv_sec * 1000 + tp.tv_usec / 1000;
                        };
                        long lastTick = calcMs();
                        statusResponse.setOnChange([&](const json &$data) {
                            long tick = calcMs();
                            if (tick > (lastTick + 10)) {
                                lastTick = tick;
                                asyncProcess($data);
                            }
                        });

                        shared_ptr<IResponse> response = ResponseFactory::getResponse(statusResponse);

                        LogIt::Info("Found {0} resources.", resources.size());
                        int resCounter = 0;
                        if (!runOnly) {
                            for (auto &resource : resources) {
                                resCounter++;
                                if (boost::iequals(String::ToLowerCase(resource.getType()), "online") || resource.getType().empty()) {
                                    step = 1;
                                    queue->consume_all([](auto a) {});
                                    string path = this->resourcesUpdater->DownloadResource(resource.getLocation(), resource, asyncProcess);
                                    if (!path.empty() && FileSystem::Exists(path)) {
                                        string destPath = (fspath(basePath) / fspath(path).filename()).string();
                                        if (resource.isCopyOnly()) {
                                            step = 2;
                                            queue->consume_all([](auto a) {});
                                            FileSystem::Copy(path, destPath, [&](bool $status) {
                                                if (!$status) {
                                                    LogIt::Error("Resource file {0} can not be copied to {1}.", path, destPath);
                                                }
                                            });
                                        } else {
                                            step = 3;
                                            queue->consume_all([](auto a) {});
                                            asyncProcess({});
                                            statusResponse.setOnComplete([&](const string &$output) {
                                                if ($output.empty()) {
                                                    LogIt::Error("Resource archive {0} extraction failed.", path);
                                                }
                                            });
                                            FileSystem::Unpack(path,
                                                               {
                                                                       {"output",     basePath},
                                                                       {"cwd",        Application::getAppDataPath() +
                                                                                      "/resource/libs/7zip"},
                                                                       {"pathTo7zip", "7za.exe"}
                                                               },
                                                               response);
                                        }
                                    } else {
                                        LogIt::Error("Download of {0} failed.", resource.getLocation());
                                    }
                                } else if (boost::iequals(boost::to_lower_copy(resource.getType()), "resources")) {
                                    LogIt::Info("Processing embedded resource: {0}", resource.ToString());
                                    queue->consume_all([](auto a) {});
                                    ResourcesManager::ReadResource(resource.getName(), [&](const char *$data, unsigned int $length) {
                                        boost::filesystem::path outPath = boost::filesystem::path(
                                                Application::getAppDataPath() + "/" + resource.getLocation());
                                        outPath = outPath.normalize();
                                        if (!FileSystem::Exists(outPath.parent_path().string())) {
                                            FileSystem::CreateDirectory(outPath.parent_path().string());
                                        }
                                        step = 2;
                                        FileSystem::Write(outPath.string(), string($data, $length));
                                        if (!resource.isCopyOnly()) {
                                            step = 3;
                                            statusResponse.setOnComplete([&](const string &$output) {
                                                if (!$output.empty()) {
                                                    LogIt::Error("Unpack of resource {0} failed.", resource.getName());
                                                }
                                            });
                                            FileSystem::Unpack(outPath.string(), {
                                                    {"output",     basePath},
                                                    {"cwd",        Application::getAppDataPath() + "/resource/libs/7zip"},
                                                    {"pathTo7zip", "7za.exe"}
                                            }, response);
                                        } else {
                                            asyncProcess(
                                                    {
                                                            {"type", "onchange"},
                                                            {"data", {
                                                                             {"currentValue", resCounter},
                                                                             {"rangeEnd", resources.size()},
                                                                             {"rangeStart", 0}
                                                                     }}
                                                    });
                                            FileSystem::Copy(outPath.string(), basePath + "/" + resource.getLocation(), [&](bool $status) {
                                                if (!$status) {
                                                    LogIt::Error("Copy of resource {0} failed.", resource.getName());
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    LogIt::Warning("Unexpected resource type {0} for resource location: {1}.", resource.getType(),
                                                   resource.getLocation());
                                }
                            }
                        } else {
                            LogIt::Info("Resources processing skipped because application run-only is required.");
                        }

                        if (!executablePath.empty()) {
                            if (args.isPrintAppPath()) {
                                std::cout << "{app-path: \""
                                          << fspath(basePath + "/" + executablePath).normalize().make_preferred().string()
                                          << "\"}" << std::endl;
                            }

                            if (!args.isSkipExecute()) {
                                if (!args.getAppArgs().empty()) {
                                    executableArgs.emplace_back(args.getAppArgs());
                                }
                                LogIt::Info("Starting application {0} in \"{1}\" with args: {2}", executablePath, basePath,
                                            ArrayList::Join(executableArgs));
                                step = 0;
                                queue->push(String::Format(Localization::getInstance().getLocalized("starting"), executablePath));
                                executablePath = fspath(basePath + "/" + executablePath).normalize().make_preferred().string();

                                queue->consume_all([](auto a) {});
                                queue->push("!hide");

                                Child::Spawn(executablePath, executableArgs, SpawnOptions(basePath, {}, false, true));
                            } else {
                                LogIt::Info("Execute skipped due to CLI switch.");
                            }
                        } else {
                            LogIt::Warning("Executable path has not been defined. Launching the application was skipped.");
                        }

                        LogIt::Info("Resource processing task has finished.");
                    } else {
                        LogIt::Error("Resource preparation skipped due to error in install path preparation.");
#ifdef WIN_PLATFORM
                        MessageBox(nullptr,
                                   String::Format(Localization::getInstance().getLocalized("installPreparationFailedText"),
                                                  basePath).c_str(),
                                   Localization::getInstance().getLocalized("installPreparationFailedCaption").c_str(),
                                   MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
#endif
                    }

                    CacheManager::getInstance().Save();
                    boost::this_thread::sleep_for(boost::chrono::seconds(4));

                    queue->consume_all([](auto a) {});
                    queue->push("!quit");

                    LogIt::Info("Application processing thread finished.");
                });

                threadPool.Execute();

                if (!args.isHeadless()) {
#ifdef WIN_PLATFORM
                    Gdiplus::GdiplusShutdown(gdiPlusToken);  // finalize GDI+
#endif
                }
            }

            CacheManager::getInstance().Save();

            LogIt::Info("Application closed.");
            std::cout << "SelfExtractor finished." << std::endl;
        } else if (parseStatus != 1) {
            LogIt::Error("Can not parse CLI arguments.");
        }

        return 0;
    }

    bool Application::prepareIntern() {
        bool retState = true;
        json data = ResourcesManager::FindConfig("INTERN_CONF_JSON");

        if (!data.is_null() && !data.empty()) {
            for (json::const_iterator it = data.cbegin(); it != data.cend(); ++it) {
                ResourceInfo info(it.value());
                LogIt::Info("Processing internal resource: {0}", it.value().dump());
                if (boost::iequals(info.getType(), "RESOURCES")) {
                    ResourcesManager::ReadResource(info.getName(), [&](const char *$data, const unsigned int $length) {
                        boost::filesystem::path outPath = boost::filesystem::path(Application::getAppDataPath() + "/" + info.getLocation());
                        if (!boost::filesystem::exists(outPath)) {
                            outPath = outPath.normalize();
                            if (!FileSystem::Exists(outPath.parent_path().string())) {
                                FileSystem::CreateDirectory(outPath.parent_path().string());
                            }
                            FileSystem::Write(outPath.string(), string($data, $length));
                        } else {
                            LogIt::Info("Internal resource already exists in \"{0}\".", outPath);
                        }
                    });
                } else {
                    LogIt::Warning("Skipped {0}. Internal resource should not contains other than RESOURCES type.",
                                   info.getLocation());
                }
            }
        } else {
            retState = false;
            LogIt::Error("No internal configuration found in executable resources (intern.conf.json).");
        }

        return retState;
    }

    bool Application::prepareConfig(json &$config) {
        bool retState = false;

        // load default selfextractor config and localization
        $config = JSON::ParseJsonp(FileSystem::Read(
                "resource/data/Io/Oidis/SelfExtractor/Configuration/BaseSelfExtractor.jsonp"));
        Localization::getInstance().setLocData(JSON::ParseJsonp(FileSystem::Read(
                "resource/data/Io/Oidis/SelfExtractor/Localization/BaseSelfExtractorLocalization.jsonp")));

        // load selfextractor config
        json embConfig = ResourcesManager::FindConfig("SELFEXTRACTOR_CONF_JSON");
        if (!embConfig.empty() && embConfig.is_object()) {
            LogIt::Debug("Embedded config:\n{0}", embConfig.dump(2));
            if (embConfig.find("url") == embConfig.end() || embConfig["url"].get<string>().empty()) {
                LogIt::Info("SelfExtractor uses offline (embedded) configuration.");
                $config = JSON::CombineJson($config, embConfig);

                LogIt::Info("Loading internal localization.");
                json locData = JSON::CombineJson(Localization::getInstance().getLocData(),
                                                 JSON::ParseJsonp(ResourcesManager::ReadConfig("LANG")));
                Localization::getInstance().setLocData(locData);

                retState = true;
            } else {
                boost::filesystem::path configFilePath = boost::filesystem::path(Application::getAppDataPath()) / ("config.json");
                boost::filesystem::path locFilePath = boost::filesystem::path(Application::getAppDataPath()) / ("lang.json");

                string url = embConfig.value("url", string());
                LogIt::Info("SelfExtractor uses online configuration from \"{0}\".", url);

                json options = {
                        {"method",       "GET"},
                        {"url",          url},
                        {"headers",      {
                                                 {"charset", "UTF-8"},
                                                 {"User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                                "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}
                                         }
                        },
                        {"body",         ""},
                        {"streamOutput", true}
                };

                $config = this->resourcesUpdater->DownloadConfig(options, $config);
                if ($config.empty() || $config.is_null() || !$config.is_object()) {
                    if (!FileSystem::Exists(configFilePath.string()) || CacheManager::getInstance().isDisabled()) {
                        LogIt::Error("Can not download configuration from specified link \"{0}\".", url);
#ifdef WIN_PLATFORM
                        MessageBox(nullptr,
                                   String::Format(Localization::getInstance().getLocalized("downloadingFailedText"), url).c_str(),
                                   Localization::getInstance().getLocalized("downloadingFailedCaption").c_str(),
                                   MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
#endif
                    } else {
                        $config = json::parse(FileSystem::Read(configFilePath.string()));
                        LogIt::Info("Configuration loaded from already existing \"{0}\"", configFilePath.string());
                        retState = true;
                    }
                } else {
                    FileSystem::Write(configFilePath.string(), $config.dump(2));
                    retState = true;
                }

                string locUrl = $config.value("localization", "");
                if (!locUrl.empty()) {
                    options["url"] = $config["localization"];
                    json locData = this->resourcesUpdater->DownloadConfig(options, Localization::getInstance().getLocData());
                    if (!locData.empty() || locData.is_object()) {
                        FileSystem::Write(locFilePath.string(), locData.dump(2));
                        Localization::getInstance().setLocData(locData);
                    } else {
                        if (!FileSystem::Exists(locFilePath.string()) || CacheManager::getInstance().isDisabled()) {
                            LogIt::Error("Can not download localization data from specified link \"{0}\".", locUrl);
                        } else {
                            json loc = json::parse(FileSystem::Read(locFilePath.string()));
                            Localization::getInstance().setLocData(loc);
                        }
                    }
                } else {
                    LogIt::Warning("Localization is not defined in application configuration.");
                }
            }
        } else {
            LogIt::Error("No valid embedded configuration found.");
        }
        return retState;
    }

    const string &Application::getAppDataPath() {
        return appDataPath;
    }

    void Application::setAppDataPath(const string &$appDataPath) {
        appDataPath = $appDataPath;
    }

    string Application::ResolveOriginalFileName(const string &$path) {
        string retVal;
#ifdef WIN_PLATFORM
        string path = FileSystem::NormalizePath($path);
        DWORD versionInfoSize = GetFileVersionInfoSizeA(path.c_str(), nullptr);
        auto *versionInfoBuffer = new BYTE[versionInfoSize];
        GetFileVersionInfoA(path.c_str(), 0, versionInfoSize, versionInfoBuffer);

        typedef struct {
            WORD wLanguage;
            WORD wCodePage;
        } LanguageCodePage;
        UINT translateInfoSize;
        LanguageCodePage *translateInfo;

        VerQueryValueA(versionInfoBuffer,
                       "\\VarFileInfo\\Translation",
                       reinterpret_cast<LPVOID *>(&translateInfo),
                       &translateInfoSize);

        for (int i = 0; i < static_cast<int>(translateInfoSize / sizeof(LanguageCodePage)); i++) {
            char subBlock[256];
            if (SUCCEEDED(StringCchPrintfA(subBlock, 255,
                                           "\\StringFileInfo\\%04x%04x\\ProductName",
                                           translateInfo[i].wLanguage,
                                           translateInfo[i].wCodePage))) {
                LPSTR value;
                UINT inNameSize;
                if (VerQueryValueA(versionInfoBuffer, subBlock, reinterpret_cast<LPVOID *>(&value), &inNameSize) != 0) {
                    retVal = string(value);
                    retVal = boost::trim_copy(string(value));
                }
            }
        }
        delete[] versionInfoBuffer;
#endif
        return retVal;
    }
}
