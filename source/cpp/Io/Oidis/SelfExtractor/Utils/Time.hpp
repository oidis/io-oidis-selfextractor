/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_UTILS_TIME_HPP_
#define IO_OIDIS_SELFEXTRACTOR_UTILS_TIME_HPP_

namespace Io::Oidis::SelfExtractor::Utils {
    /**
     * Time utility class contains advanced features for operations with time.
     */
    class Time : private Io::Oidis::XCppCommons::Interfaces::INonCopyable,
                 private Io::Oidis::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Get unix timestamp of specified $time or from current time.
         * @param $time Specify time or leave default to use current time.
         * @return Returns unix timestamp.
         */
        static int getTimestamp(const boost::posix_time::ptime &$time = boost::posix_time::second_clock::universal_time());
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_UTILS_TIME_HPP_
