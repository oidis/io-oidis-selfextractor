/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#ifdef WIN_PLATFORM

#include <Shobjidl.h>

#endif

namespace Io::Oidis::SelfExtractor::UserControls {
    using Io::Oidis::SelfExtractor::Structures::Size;
    using Io::Oidis::SelfExtractor::Structures::Position;
    using Io::Oidis::XCppCommons::Primitives::String;

    SplashScreen::SplashScreen(json const &$config)
            : BaseGuiObject($config) {
        if (!$config.empty() && $config.is_object()) {
            this->setBackground($config.value("background", this->background));
            this->setInstallPath($config.value("installPath", this->installPath));
            this->setExecutable($config.value("executable", this->executable));
            this->setExecutableArgs($config.value("executableArgs", this->executableArgs));

            if ($config.find("userControls") != $config.end()) {
                json userControls = $config["userControls"];
                if (userControls.find("label") != userControls.end()) {
                    this->label.reset(new Label(userControls["label"]));
                }
                if (userControls.find("labelStatic") != userControls.end()) {
                    this->labelStatic.reset(new Label(userControls["labelStatic"]));
                }
                if (userControls.find("progress") != userControls.end()) {
                    this->progressBar.reset(new ProgressBar(userControls["progress"]));
                }
            }

            if ($config.find("resources") != $config.end()) {
                json res = $config["resources"];
                for (json::iterator it = res.begin(); it != res.end(); ++it) {
                    this->resources.push_back(Structures::ResourceInfo(it.value()));
                }
            }
        }
    }

    void SplashScreen::Update() {
#ifdef WIN_PLATFORM
        POINT ptZero = {0};

        HDC hdcScreen = GetDC(nullptr);
        HDC hdcMem = CreateCompatibleDC(hdcScreen);
        HBITMAP hBitmap = CreateCompatibleBitmap(hdcScreen, this->getSize().getWidth(), this->getSize().getHeight());
        auto hbmpOld = (HBITMAP)SelectObject(hdcMem, hBitmap);

        Gdiplus::Graphics *graphics = Gdiplus::Graphics::FromHDC(hdcMem);

        this->Draw(*graphics);

        BLENDFUNCTION blend = {0};
        blend.BlendOp = AC_SRC_OVER;
        blend.SourceConstantAlpha = 255;
        blend.AlphaFormat = AC_SRC_ALPHA;

        SIZE sizeSplash = {static_cast<LONG>(this->getSize().getWidth()), static_cast<LONG>(this->getSize().getHeight())};
        UpdateLayeredWindow(this->hwnd, hdcScreen, nullptr, &sizeSplash, hdcMem, &ptZero, RGB(0, 0, 0), &blend, ULW_ALPHA);

        delete graphics;
        DeleteObject(hbmpOld);
        DeleteObject(hBitmap);
        SelectObject(hdcMem, hbmpOld);
        DeleteDC(hdcMem);
        ReleaseDC(nullptr, hdcScreen);
#endif
    }

#ifdef WIN_PLATFORM
    ITaskbarList3 *taskbarList3;
    ITaskbarList *taskbarList;
#endif

    void SplashScreen::Create() {
#ifdef WIN_PLATFORM
        HINSTANCE hinstance = GetModuleHandle(nullptr);
        const std::string windowTitle = "SelfExtractor";
        const std::string windowClass = "SelfExtractor";

        static bool isClassRegistered = false;
        if (!isClassRegistered) {
            isClassRegistered = true;

            WNDCLASSEX wndclassex = {};
            wndclassex.cbSize = sizeof(WNDCLASSEX);
            wndclassex.style = CS_HREDRAW | CS_VREDRAW;
            wndclassex.lpfnWndProc = WndProc;
            wndclassex.cbClsExtra = 0;
            wndclassex.cbWndExtra = 0;
            wndclassex.hInstance = hinstance;
            wndclassex.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(0));
            wndclassex.hCursor = LoadCursor(nullptr, IDC_ARROW);
            wndclassex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
            wndclassex.lpszMenuName = nullptr;
            wndclassex.lpszClassName = static_cast<LPCSTR>(windowClass.c_str());
            wndclassex.hIconSm = LoadIcon(hinstance, MAKEINTRESOURCE(0));

            RegisterClassEx(&wndclassex);
        }

        HMONITOR monitor = MonitorFromWindow(nullptr, MONITOR_DEFAULTTOPRIMARY);
        MONITORINFO info = {};
        info.cbSize = sizeof(info);
        GetMonitorInfo(monitor, &info);

        this->hwnd = CreateWindowEx(WS_EX_LAYERED, static_cast<LPCSTR>(windowClass.c_str()), static_cast<LPCSTR>(windowTitle.c_str()),
                                    WS_POPUP,
                                    (info.rcMonitor.right - info.rcMonitor.left - this->getSize().getWidth()) / 2 + info.rcMonitor.left,
                                    (info.rcMonitor.bottom - info.rcMonitor.top - this->getSize().getHeight()) / 2 + info.rcMonitor.top,
                                    this->getSize().getWidth(), this->getSize().getHeight(),
                                    nullptr, nullptr, hinstance, nullptr);

        SetWindowLongPtr(this->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
        SetMenu(this->hwnd, nullptr);

        if (!String::StartsWith(this->background, "#") && !String::StartsWith(this->background, "0x")) {
            this->bgImage.reset(new Image());
            this->bgImage->Load(this->background);
        }

        ShowWindow(this->hwnd, SW_SHOWNORMAL);
        UpdateWindow(this->hwnd);
        Update();

        CoInitialize(nullptr);

        CoCreateInstance(CLSID_TaskbarList, nullptr, CLSCTX_INPROC_SERVER, IID_ITaskbarList3, reinterpret_cast<void **>(&taskbarList3));
        CoCreateInstance(CLSID_TaskbarList, nullptr, CLSCTX_INPROC_SERVER, IID_ITaskbarList, reinterpret_cast<void **>(&taskbarList));

        if (this->progressBar != nullptr && !this->progressBar->isMarquee() && this->progressBar->isTrayProgress()) {
            taskbarList3->SetProgressState(this->hwnd, TBPF_NORMAL);
            taskbarList3->SetProgressValue(this->hwnd, 0, 100);
        } else {
            taskbarList3->SetProgressState(this->hwnd, TBPF_NOPROGRESS);
        }

        SetTimer(this->hwnd, 0, 10, nullptr);
#endif
    }

#ifdef WIN_PLATFORM

    LRESULT CALLBACK SplashScreen::WndProc(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam) {
        auto *self = reinterpret_cast<SplashScreen *>(GetWindowLongPtr($hwnd, GWLP_USERDATA));
        if ((self == nullptr) || self->hwnd != $hwnd) {
            return DefWindowProc($hwnd, $message, $wParam, $lParam);
        }

        switch ($message) {
            case WM_CLOSE: {
                PostQuitMessage(0);
                break;
            }
            case WM_DESTROY: {
                KillTimer(self->hwnd, 0);
                SetWindowLongPtr(self->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(nullptr));
                self->hwnd = nullptr;
                break;
            }
            case WM_TIMER: {
                if ($wParam == 0) {
                    if (self->commandQueue != nullptr) {
                        if (self->loops++ >= 0) {
                            self->loops = 0;
                            string cmd;
                            string progress;
                            string text;
                            while (self->commandQueue->pop(cmd)) {
                                if (boost::iequals("!quit", cmd)) {
                                    self->destroy = true;
                                    break;
                                } else if (String::StartsWith(cmd, "!progress:")) {
                                    progress = cmd;
                                } else if (boost::iequals("!hide", cmd)) {
                                    taskbarList->DeleteTab(self->hwnd);
                                } else {
                                    text = cmd;
                                }
                            }
                            if (!self->destroy) {
                                if (!progress.empty()) {
                                    string val = String::Remove(progress, "!progress:");
                                    int index = String::IndexOf(val, "/");
                                    try {
                                        int current = boost::lexical_cast<int>(val.substr(0, static_cast<unsigned int>(index)));
                                        int max = boost::lexical_cast<int>(val.substr(static_cast<unsigned int>(index + 1)));
                                        if (self->progressBar != nullptr) {
                                            self->getProgressBar()->setValue(current);
                                            self->getProgressBar()->setMax(max);
                                            self->getProgressBar()->setMin(0);
                                            if (!self->getProgressBar()->isMarquee() && self->getProgressBar()->isTrayProgress()) {
                                                taskbarList3->SetProgressValue(self->hwnd, static_cast<ULONGLONG>(current),
                                                                               static_cast<ULONGLONG>(max));
                                            }
                                        }
                                    } catch (std::exception &ex) {
                                        std::cout << ex.what() << std::endl;
                                    }
                                }
                                if (!text.empty()) {
                                    self->getLabel()->setText(text);
                                }
                            }
                        }
                    }

                    if (self->destroy) {
                        DestroyWindow(self->hwnd);
                        PostQuitMessage(0);
                    } else {
                        self->Update();
                    }
                }
                break;
            }
            default: {
                return DefWindowProc($hwnd, $message, $wParam, $lParam);
            }
        }

        return 0;
    }

    void SplashScreen::Draw(Gdiplus::Graphics &$graphics) {
        if (this->isVisible()) {
            if (this->bgImage != nullptr) {
                $graphics.Clear(Gdiplus::Color::Transparent);
            } else {
                $graphics.Clear(Structures::Color(this->background).getColor());
            }
            if (this->bgImage != nullptr) {
                this->bgImage->Draw($graphics);
            }

            if (this->progressBar != nullptr) {
                this->progressBar->Draw($graphics);
            }
            if (this->labelStatic != nullptr) {
                this->labelStatic->Draw($graphics);
            }
            if (this->label != nullptr) {
                this->label->Draw($graphics);
            }
        }
    }

    HWND SplashScreen::getHwnd() const {
        return this->hwnd;
    }

#endif

    const string &SplashScreen::getBackground() const {
        return this->background;
    }

    void SplashScreen::setBackground(const string &$background) {
        this->background = $background;
    }

    const string &SplashScreen::getExecutable() const {
        return this->executable;
    }

    void SplashScreen::setExecutable(const string &$executable) {
        this->executable = $executable;
    }

    const string &SplashScreen::getInstallPath() const {
        return this->installPath;
    }

    void SplashScreen::setInstallPath(const string &$installPath) {
        this->installPath = $installPath;
    }

    const shared_ptr<Label> &SplashScreen::getLabel() const {
        return this->label;
    }

    void SplashScreen::setLabel(shared_ptr<Label> const &$label) {
        this->label = $label;
    }

    const shared_ptr<Label> &SplashScreen::getLabelStatic() const {
        return this->labelStatic;
    }

    void SplashScreen::setLabelStatic(shared_ptr<Label> const &$labelStatic) {
        this->labelStatic = $labelStatic;
    }

    const shared_ptr<ProgressBar> &SplashScreen::getProgressBar() const {
        return this->progressBar;
    }

    void SplashScreen::setProgressBar(shared_ptr<ProgressBar> const &$progressBar) {
        this->progressBar = $progressBar;
    }

    const std::vector<Structures::ResourceInfo> &SplashScreen::getResources() const {
        return this->resources;
    }

    void SplashScreen::setResources(std::vector<Structures::ResourceInfo> const &$resources) {
        this->resources = $resources;
    }

    void SplashScreen::Destroy() {
        this->destroy = true;
    }

    void SplashScreen::setCommandQueue(const shared_ptr<boost::lockfree::spsc_queue<string>> &$commandQueue) {
        this->commandQueue = $commandQueue;
    }

    const std::vector<string> &SplashScreen::getExecutableArgs() const {
        return this->executableArgs;
    }

    void SplashScreen::setExecutableArgs(const std::vector<string> &$executableArgs) {
        this->executableArgs = $executableArgs;
    }
}
