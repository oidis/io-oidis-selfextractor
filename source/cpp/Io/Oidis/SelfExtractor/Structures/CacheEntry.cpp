/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {

    const string &CacheEntry::getKey() const {
        return this->key;
    }

    void CacheEntry::setKey(const string &$key) {
        this->key = $key;
    }

    long CacheEntry::getTimestamp() const {
        return this->timestamp;
    }

    void CacheEntry::setTimestamp(long $timestamp) {
        this->timestamp = $timestamp;
    }

    const string &CacheEntry::getData() const {
        return this->data;
    }

    void CacheEntry::setData(const string &$data) {
        this->data = $data;
    }

    CacheEntry::CacheEntry(const string &$key, const string &$data, long $timestamp)
            : key($key),
              data($data),
              timestamp($timestamp) {
    }

    CacheEntry::CacheEntry(const json &$data) {
        this->FromJson($data);
    }

    json CacheEntry::ToJson() const {
        json obj = {
                {"key",       this->key},
                {"data",      this->data},
                {"timestamp", this->timestamp}
        };
        return obj;
    }

    void CacheEntry::FromJson(const json &$data) {
        if (!$data.empty() && $data.is_object()) {
            this->key = $data.value("key", "");
            this->data = $data.value("data", "");
            this->timestamp = $data.value("timestamp", 0);
        }
    }

    std::ostream &operator<<(std::ostream &$os, const CacheEntry &$entry) {
        $os << $entry.ToJson();
        return $os;
    }

    bool CacheEntry::Empty() {
        return this->key.empty();
    }
}
