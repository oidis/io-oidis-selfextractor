/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_POSITION_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_POSITION_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * Position class stores vertical and horizontal values.
     */
    class Position {
     public:
        /**
         * Constructs default position.
         */
        Position();

        /**
         * Constructs position from values.
         * @param $x Specify horizontal position.
         * @param $y Specify vertical position.
         */
        Position(int $x, int $y);

        /**
         * @return Returns horizontal part of position.
         */
        int getX() const;

        /**
         * @param $x Specify horizontal part of position.
         */
        void setX(int $x);

        /**
         * @return Returns vertical part of position.
         */
        int getY() const;

        /**
         * @param $y Specify vertical part of position.
         */
        void setY(int $y);

        bool operator<(const Position &$rhs) const;

        bool operator>(const Position &$rhs) const;

        bool operator<=(const Position &$rhs) const;

        bool operator>=(const Position &$rhs) const;

        bool operator==(const Position &$rhs) const;

        bool operator!=(const Position &$rhs) const;

        friend std::ostream &operator<<(std::ostream &$os, const Position &$position);

     private:
        int x = 0;
        int y = 0;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_POSITION_HPP_
