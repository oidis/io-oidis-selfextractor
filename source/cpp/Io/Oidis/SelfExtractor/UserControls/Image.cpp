/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
#ifdef WIN_PLATFORM

    void Image::Draw(Gdiplus::Graphics &$graphics) {
        if (this->isVisible()) {
            if (this->isGif) {
                if (this->tick == -1 || this->tick <= (this->loopDelay * this->nativeTick)) {
                    GUID guid = Gdiplus::FrameDimensionTime;
                    this->nativeImage->SelectActiveFrame(&guid, static_cast<unsigned>(this->frameIndex));
                    this->tick = reinterpret_cast<unsigned *>(this->propertyItem[0].value)[this->frameIndex] * 10;
                    this->frameIndex = (this->frameIndex + 1) % this->framesCnt;
                    this->loopDelay = 1;
                } else {
                    this->loopDelay++;
                }
            }

            Gdiplus::ImageAttributes attributes;
            Gdiplus::Rect dest(this->getPosition().getX(), this->getPosition().getY(), this->getSize().getWidth(),
                               this->getSize().getHeight());

            $graphics.DrawImage(this->nativeImage.get(), dest, 0, 0,
                                this->getSize().getWidth(), this->getSize().getHeight(), Gdiplus::Unit::UnitPixel, &attributes);
        }
    }

#endif

    void Image::Load(const string &$path) {
#ifdef WIN_PLATFORM
        this->isGif = false;
        this->nativeImage.reset(new Gdiplus::Image(std::wstring($path.begin(), $path.end()).c_str()));
        this->setSize(Structures::Size(this->nativeImage->GetWidth(), this->nativeImage->GetHeight()));

        // check gif
        if (Io::Oidis::XCppCommons::Primitives::String::EndsWith($path, ".gif")) {
            this->isGif = true;
            unsigned count = this->nativeImage->GetFrameDimensionsCount();
            auto *dimIds = new GUID[count];
            this->nativeImage->GetFrameDimensionsList(dimIds, count);
            WCHAR strGuid[39];
            StringFromGUID2(dimIds[0], strGuid, 39);
            this->framesCnt = this->nativeImage->GetFrameCount(&dimIds[0]);

            unsigned total = this->nativeImage->GetPropertyItemSize(PropertyTagFrameDelay);

            if (this->propertyItem != nullptr) {
                free(this->propertyItem);
            }
            this->propertyItem = (Gdiplus::PropertyItem *)malloc(total);
            this->nativeImage->GetPropertyItem(PropertyTagFrameDelay, total, propertyItem);

            delete[] dimIds;
        }
#endif
    }

    Image::~Image() {
#ifdef WIN_PLATFORM
        if (this->propertyItem != nullptr) {
            free(this->propertyItem);
        }
#endif
    }
}
