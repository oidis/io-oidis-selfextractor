/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_SPLASHSCREEN_HPP_
#define IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_SPLASHSCREEN_HPP_

namespace Io::Oidis::SelfExtractor::UserControls {
    /**
     * SplashScreen control defines application window and holds each GUI elements.
     * Redrawing engine is also part of this class.
     */
    class SplashScreen : public Io::Oidis::SelfExtractor::Primitives::BaseGuiObject {
     public:
        /**
         * Constructs default SplashScreen.
         */
        SplashScreen() = default;

        /**
         * Constructs SplashScreen from JSON configuration.
         * @param $config
         */
        explicit SplashScreen(const json &$config);

        /**
         * Release internal context.
         */
        ~SplashScreen() = default;

        /**
         * Creates SplashScreen window and initialize redrawing engine.
         */
        void Create();

        /**
         * @return Returns background image path or color string.
         */
        const string &getBackground() const;

        /**
         * @param $background Specify background image path or color string.
         */
        void setBackground(const string &$background);

        /**
         * @return Returns executable path.
         */
        const string &getExecutable() const;

        /**
         * @param $executable Specify executable path.
         */
        void setExecutable(const string &$executable);

        /**
         * @return Returns executable arguments.
         */
        const std::vector<string> &getExecutableArgs() const;

        /**
         * @param $executableArgs Specify executable arguments.
         */
        void setExecutableArgs(const std::vector<string> &$executableArgs);

        /**
         * @return Returns installation base path.
         */
        const string &getInstallPath() const;

        /**
         * @param $installPath Specify installation base path.
         */
        void setInstallPath(const string &$installPath);

        /**
         * @return Returns label control.
         */
        const shared_ptr<Label> &getLabel() const;

        /**
         * @param $label Specify new label control.
         */
        void setLabel(shared_ptr<Label> const &$label);

        /**
         * @return Returns static label control.
         */
        const shared_ptr<Label> &getLabelStatic() const;

        /**
         * @param $labelStatic Specify new static label control.
         */
        void setLabelStatic(shared_ptr<Label> const &$labelStatic);

        /**
         * @return Returns progress bar control.
         */
        const shared_ptr<ProgressBar> &getProgressBar() const;

        /**
         * @param $progressBar Specify new progress bar control.
         */
        void setProgressBar(shared_ptr<ProgressBar> const &$progressBar);

        /**
         * @return Returns resources list.
         */
        const std::vector<Structures::ResourceInfo> &getResources() const;

        /**
         * @param $resources Specify new resources list.
         */
        void setResources(std::vector<Structures::ResourceInfo> const &$resources);

#ifdef WIN_PLATFORM

        /**
         * @return Returns owned window handle or NULL if not created.
         */
        HWND getHwnd() const;

        void Draw(Gdiplus::Graphics &$graphics) override;

#endif

        /**
         * Destroy window.
         */
        void Destroy();

        /**
         * @param $commandQueue Specify thread-safe command queue.
         */
        void setCommandQueue(const shared_ptr<boost::lockfree::spsc_queue<string>> &$commandQueue);

     private:
#ifdef WIN_PLATFORM
        HWND hwnd;

        static LRESULT CALLBACK WndProc(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

#endif
        string background = "#ffffff";
        string executable;
        std::vector<string> executableArgs = {};
        Io::Oidis::SelfExtractor::Structures::Size size = {900, 600};
        string installPath;
        bool destroy = false;
        int loops = 0;
        shared_ptr<boost::lockfree::spsc_queue<string>> commandQueue = nullptr;

        shared_ptr<Io::Oidis::SelfExtractor::UserControls::Label> label = nullptr;
        shared_ptr<Io::Oidis::SelfExtractor::UserControls::Label> labelStatic = nullptr;
        shared_ptr<Io::Oidis::SelfExtractor::UserControls::ProgressBar> progressBar = nullptr;
        shared_ptr<Io::Oidis::SelfExtractor::UserControls::Image> bgImage = nullptr;
        std::vector<Io::Oidis::SelfExtractor::Structures::ResourceInfo> resources;

        void Update();
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_SPLASHSCREEN_HPP_
