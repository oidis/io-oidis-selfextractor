/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_CACHEENTRY_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_CACHEENTRY_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * CacheEntry class defines simply cache entry object.
     */
    class CacheEntry {
     public:
        /**
         * Constructs default (empty) cache entry.
         */
        CacheEntry() = default;

        /**
         * Constructs cache entry from data.
         * @param $data Specify json data.
         */
        explicit CacheEntry(const json &$data);

        /**
         * Constructs cache entry form properies.
         * @param $key Specify unique entry key.
         * @param $data Specify data.
         * @param $timestamp Specify timestamp.
         */
        CacheEntry(const string &$key, const string &$data, long $timestamp);

        /**
         * @return Returns entry key.
         */
        const string &getKey() const;

        /**
         * @param $key Specify entry key.
         */
        void setKey(const string &$key);

        /**
         * @return Returns entry timestamp.
         */
        long getTimestamp() const;

        /**
         * @param $timestamp Specify entry timestamp.
         */
        void setTimestamp(long $timestamp);

        /**
         * @return Returns entry data.
         */
        const string &getData() const;

        /**
         * @param $data Specify entry data.
         */
        void setData(const string &$data);

        /**
         * Converts cache entry to JSON object.
         * @return Returns JSON object or empty.
         */
        json ToJson() const;

        /**
         * Update cache entry from JSON data.
         * @param $data Specify JSON data to be used as interns replacementd.
         */
        void FromJson(const json &$data);

        /**
         * @return Returns true if cache entry is empty, false otherwise.
         */
        bool Empty();

        friend std::ostream &operator<<(std::ostream &$os, const CacheEntry &$entry);

     private:
        string key;
        string data;
        long timestamp = 0;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_CACHEENTRY_HPP_
