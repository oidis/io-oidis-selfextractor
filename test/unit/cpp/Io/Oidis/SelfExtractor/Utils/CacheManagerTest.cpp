/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::SelfExtractor::Structures::CacheEntry;
using Io::Oidis::XCppCommons::System::IO::FileSystem;

namespace Io::Oidis::SelfExtractor::Utils {
    class CacheManagerTest
            : public testing::Test {
     protected:
        void SetUp() override {
            CacheManager::getInstance().Init(this->testDataDir + "/cache.json");
            this->backupPath = CacheManager::getInstance().getCacheFilePath();
        }

        void TearDown() override {
            CacheManager::getInstance().setCacheFilePath(this->backupPath);
        }

     private:
        string backupPath;

     public:
        string testDataDir = "../../test/resource/data/Io/Oidis/SelfExtractor/Utils";
    };

    TEST_F(CacheManagerTest, Load_Save) {
        CacheManager::getInstance().setCacheFilePath(this->testDataDir + "/cache2.json");
        CacheManager::getInstance().Save();

        json original = json::parse(FileSystem::Read(this->testDataDir + "/cache.json"));
        json test = json::parse(FileSystem::Read(this->testDataDir + "/cache2.json"));

        ASSERT_STREQ(original.dump().c_str(), test.dump().c_str());

        ASSERT_TRUE(FileSystem::Delete(this->testDataDir + "/cache2.json"));
    }

    TEST_F(CacheManagerTest, getEntry) {
        ASSERT_STREQ("{\"data\":\"data2\",\"key\":\"key2\",\"timestamp\":22}",
                     CacheManager::getInstance().getEntry("key2").ToJson().dump().c_str());
        ASSERT_STREQ("{\"data\":\"\",\"key\":\"\",\"timestamp\":0}",
                     CacheManager::getInstance().getEntry("keyXXX").ToJson().dump().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::Utils
