/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Primitives {
    class BaseObject : public BaseGuiObject {
     public:
        explicit BaseObject(json const &$config)
                : BaseGuiObject($config) {
        }
#ifdef WIN_PLATFORM
        void Draw(Gdiplus::Graphics &$graphics) override {
        }
#endif
    };

    TEST(BaseGuiObjectTest, Construction) {
        json data = {
                {"x",       111},
                {"y",       123},
                {"width",   200},
                {"height",  35},
                {"visible", true}
        };

        BaseObject baseGuiObject(data);
        ASSERT_EQ(111, baseGuiObject.getPosition().getX());
        ASSERT_EQ(123, baseGuiObject.getPosition().getY());
        ASSERT_EQ(200, baseGuiObject.getSize().getWidth());
        ASSERT_EQ(35, baseGuiObject.getSize().getHeight());
        ASSERT_TRUE(baseGuiObject.isVisible());
    }
}  // namespace Io::Oidis::SelfExtractor::Primitives
