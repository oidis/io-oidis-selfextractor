/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Utils {

    Localization &Localization::getInstance() {
        static Localization instance;
        return instance;
    }

    const json &Localization::getLocData() const {
        return this->locData;
    }

    void Localization::setLocData(const json &$locData) {
        this->locData = $locData;
    }

    string Localization::getLocalized(const string &$key) {
        string retVal = "";
        if (!this->locData.empty() && this->locData.is_object()) {
            retVal = this->locData.value($key, "");
        }
        return retVal;
    }
}
