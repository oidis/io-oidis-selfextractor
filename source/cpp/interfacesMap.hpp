/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_OIDIS_SELFEXTRACTOR_INTERFACESMAP_HPP_  // NOLINT
#define IO_OIDIS_SELFEXTRACTOR_INTERFACESMAP_HPP_

namespace Io {
    namespace Oidis {
        namespace SelfExtractor {
            class Application;
            class Loader;
            namespace Enums {
                class TextJustifyType;
            }
            namespace Primitives {
                class BaseGuiObject;
            }
            namespace ResponseApi {
                namespace Handlers {
                    class StatusResponse;
                }
            }
            namespace Structures {
                class CacheEntry;
                class Color;
                class Font;
                class Position;
                class ResourceInfo;
                class SelfExtractorArgs;
                class Size;
            }
            namespace UserControls {
                class Image;
                class Label;
                class ProgressBar;
                class SplashScreen;
            }
            namespace Utils {
                class CacheManager;
                class JSON;
                class Localization;
                class ResourcesManager;
                class ResourcesUpdater;
                class Time;
            }
        }
    }
}

#endif  // IO_OIDIS_SELFEXTRACTOR_INTERFACESMAP_HPP_  // NOLINT
