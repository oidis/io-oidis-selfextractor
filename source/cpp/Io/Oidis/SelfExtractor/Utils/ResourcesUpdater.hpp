/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESUPDATER_HPP_
#define IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESUPDATER_HPP_

namespace Io::Oidis::SelfExtractor::Utils {
    /**
     * ResourcesUpdater class contains logic to check and download resources or configurations from online server.
     */
    class ResourcesUpdater {
     public:
        /**
         * Check if update is available for resource. Could be used for SelfExtractor resources and also for intern resources
         * (i.e. image files referenced by configuration).
         * @param $info Specify resource information.
         * @param $timestamp Specify timestamp of current resource.
         * @param $version Specify version of current resource.
         * @return Returns true if new version or resource with newer timestamp is available on server.
         */
        bool UpdateAvailable(const Io::Oidis::SelfExtractor::Structures::ResourceInfo &$info, long $timestamp,
                             const string &$version);

        /**
         * Download resource from specified $url and additional information if necessary.
         * @param $url Specify url of resource.
         * @param $info Specify additional info, used for SelfExtractor resources (SelfUpdate packages).
         * @return Returns path of downloaded resource, empty if not succeed.
         */
        string DownloadResource(const string &$url, const Io::Oidis::SelfExtractor::Structures::ResourceInfo &$info = {},
                                const function<void(const json $data)> &$handler = nullptr);

        /**
         * Download configuration from specified $options with inherited configurations if available and combine it with $config.
         * @param $options Specify configuration options.
         * @param $config Specify base configuration.
         * @return Returns new configuration or $config if not found on server.
         */
        json DownloadConfig(const json &$options, const json &$config = {});

     protected:
        void DownloadSingleConfig(const json &$options, const function<void(const json &$data)> &$handler);

     private:
        string findHeaderKeyIgnoreCase(const json &$obj, const string &$key);
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_UTILS_RESOURCESUPDATER_HPP_
