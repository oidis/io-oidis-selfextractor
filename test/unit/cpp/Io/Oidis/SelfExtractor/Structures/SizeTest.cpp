/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(SizeTest, Construction) {
        Size size;
        ASSERT_EQ(0, size.getWidth());
        ASSERT_EQ(0, size.getHeight());

        size = Size(11, 13);
        ASSERT_EQ(11, size.getWidth());
        ASSERT_EQ(13, size.getHeight());

        Size size1(size);
        ASSERT_EQ(11, size1.getWidth());
        ASSERT_EQ(13, size1.getHeight());
    }

    TEST(SizeTest, Comparers) {
        Size sizeA = {15, 16};
        Size sizeB = {11, 13};
        ASSERT_TRUE(sizeA > sizeB);
        ASSERT_FALSE(sizeA < sizeB);
        ASSERT_TRUE(sizeA != sizeB);
        sizeA = sizeB;
        ASSERT_TRUE(sizeA >= sizeB);
        ASSERT_TRUE(sizeA <= sizeB);
        ASSERT_TRUE(sizeA == sizeB);
    }

    TEST(SizeTest, Output) {
        std::ostringstream oss;
        Size size{25, 33};
        oss << size;
        ASSERT_STREQ("[width, height]: [25, 33]", oss.str().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::Structures
