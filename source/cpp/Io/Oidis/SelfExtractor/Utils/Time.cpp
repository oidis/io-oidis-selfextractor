/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Utils {
    int Time::getTimestamp(const boost::posix_time::ptime &$time) {
        boost::posix_time::time_duration duration = $time - boost::posix_time::time_from_string("1970-01-01 00:00:00");
        return duration.total_seconds();
    }
}
