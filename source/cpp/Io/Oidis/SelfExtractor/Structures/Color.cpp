/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    using Io::Oidis::XCppCommons::Primitives::String;

    class CastException : std::exception {
     public:
        explicit CastException(const string &$message)
                : message($message) {}

        const char *what() const throw() override {
            return message.c_str();
        }

     private:
        string message = "";
    };

#ifdef WIN_PLATFORM

    Color::Color()
            : color(Gdiplus::Color::White) {}

#else

    Color::Color() {}

#endif


    Color::Color(const string &$value) {
        this->FromString($value);
    }

    Color::Color(int $red, int $green, int $blue) {
#ifdef WIN_PLATFORM
        this->color = Gdiplus::Color(static_cast<BYTE>(0xff), static_cast<BYTE>($red), static_cast<BYTE>($green), static_cast<BYTE>($blue));
#endif
    }

    Color::Color(int $red, int $green, int $blue, int $alpha) {
#ifdef WIN_PLATFORM
        this->color = Gdiplus::Color(static_cast<BYTE>($alpha), static_cast<BYTE>($red), static_cast<BYTE>($green),
                                     static_cast<BYTE>($blue));
#endif
    }

    void Color::FromString(const string &$value) {
#ifdef WIN_PLATFORM
        string value = boost::replace_first_copy($value, "#", "");
        value = boost::replace_first_copy(value, "0x", "");
        string a, r, g, b;
        if (value.size() == 6) {
            r = String::Substring(value, 0, 2);
            g = String::Substring(value, 2, 4);
            b = String::Substring(value, 4, 6);
            this->color = Gdiplus::Color(static_cast<BYTE>(0xff), static_cast<BYTE>(std::stoi(r, 0, 16)),
                                         static_cast<BYTE>(std::stoi(g, 0, 16)), static_cast<BYTE>(std::stoi(b, 0, 16)));
        } else if (value.size() == 8) {
            a = String::Substring(value, 0, 2);
            r = String::Substring(value, 2, 4);
            g = String::Substring(value, 4, 6);
            b = String::Substring(value, 6, 8);
            this->color = Gdiplus::Color(static_cast<BYTE>(std::stoi(a, 0, 16)), static_cast<BYTE>(std::stoi(r, 0, 16)),
                                         static_cast<BYTE>(std::stoi(g, 0, 16)), static_cast<BYTE>(std::stoi(b, 0, 16)));
        } else {
            throw CastException("Color::FromString can not convert other than 6 or 8 long string to color.");
        }
#endif
    }

    inline std::ostream &write(std::ostream &$ios, int $value) {
        return $ios << std::hex << std::noshowbase << std::setw(2) << std::setfill('0') << $value;
    }

    string Color::ToString(const string &$prefix) const {
        std::ostringstream oss;
#ifdef WIN_PLATFORM
        oss << $prefix;
        write(oss, static_cast<int>(this->color.GetA()));
        write(oss, static_cast<int>(this->color.GetR()));
        write(oss, static_cast<int>(this->color.GetG()));
        write(oss, static_cast<int>(this->color.GetB()));
#endif
        return oss.str();
    }

#ifdef WIN_PLATFORM

    Gdiplus::Color Color::getColor() const {
        return this->color;
    }

#endif
}
