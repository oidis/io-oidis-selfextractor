/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#ifndef WIN_PLATFORM

#include <libelf.h>
#include <gelf.h>

#endif

namespace Io::Oidis::SelfExtractor::Utils {
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::XCppCommons::EnvironmentArgs;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::Primitives::String;

    static string modulePath;

    json ResourcesManager::FindConfig(const string &$name) {
        string data = ResourcesManager::ReadConfig($name);
        if (!data.empty()) {
            return json::parse(data);
        }
        return {};
    }

    string ResourcesManager::ReadConfig(const string &$name) {
        string data;
#ifdef WIN_PLATFORM
        auto hModule = reinterpret_cast<HMODULE>(ResourcesManager::getModule());
        HRSRC hrsrc = FindResource(hModule, $name.c_str(), "CONFIG");
        if (hrsrc != nullptr) {
            HGLOBAL hglobal = LoadResource(hModule, hrsrc);
            if (hglobal != nullptr) {
                auto *bufPtr = static_cast<char *>(LockResource(hglobal));
                if (bufPtr != nullptr) {
                    auto size = static_cast<unsigned int>(SizeofResource(hModule, hrsrc));
                    data = string(bufPtr, size);
                } else {
                    LogIt::Error("Locked resource points to NULL for resource name \"{0}\"", $name);
                }
                FreeResource(hglobal);
            } else {
                LogIt::Error("Can not load resource defined by name \"{0}\".", $name);
            }
        } else {
            LogIt::Error("Can not find resource with name \"{0}\".", $name);
        }
#else
        using Io::Oidis::XCppCommons::EnvironmentArgs;
        using Io::Oidis::XCppCommons::Primitives::String;

        Elf *elfFile;
        Elf_Scn *section;
        Elf_Data *elfData;
        GElf_Shdr shdr;
        size_t shstrndx;
        char *sectionName;

        string exeName = EnvironmentArgs::getInstance().getExecutableName();

        int fd = open(exeName.c_str(), O_RDONLY, 0);
        if (fd < 0) {
            return string();
        }

        if (elf_version(EV_CURRENT) == EV_NONE) {
            return string();
        }

        elfFile = elf_begin(fd, ELF_C_READ, nullptr);
        if (elfFile == nullptr) {
            return string();
        }

        if (elf_kind(elfFile) != ELF_K_ELF) {
            return string();
        }

        if (elf_getshdrstrndx(elfFile, &shstrndx) != 0) {
            return string();
        }

        section = nullptr;
        while ((section = elf_nextscn(elfFile, section)) != nullptr) {
            if (gelf_getshdr(section, &shdr) != &shdr) {
                return string();
            }

            if ((sectionName = elf_strptr(elfFile, shstrndx, shdr.sh_name)) == nullptr) {
                return string();
            }

            if (String::IsEqual(string(sectionName), string(".CONFIG/") + $name)) {
                elfData = nullptr;
                elfData = elf_getdata(section, elfData);
                if (elfData != nullptr) {
                    data.assign(static_cast<char *>(elfData->d_buf), elfData->d_size);
                    break;
                }
            }
        }

        elf_end(elfFile);
        close(fd);
#endif
        return data;
    }

    void ResourcesManager::ReadResource(const string &$name, std::function<void(const char *, unsigned int)> const &$handler) {
#ifdef WIN_PLATFORM
        auto hModule = reinterpret_cast<HMODULE>(ResourcesManager::getModule());
        HRSRC hrsrc = FindResource(hModule, $name.c_str(), "RESOURCES");
        if (hrsrc != nullptr) {
            HGLOBAL hglobal = LoadResource(hModule, hrsrc);
            if (hglobal != nullptr) {
                auto *bufPtr = static_cast<char *>(LockResource(hglobal));
                if (bufPtr != nullptr) {
                    auto size = static_cast<unsigned int>(SizeofResource(hModule, hrsrc));
                    if ($handler != nullptr) {
                        $handler(bufPtr, size);
                    }
                } else {
                    LogIt::Error("Locked resource points to NULL for resource name \"{0}\"", $name);
                }
                FreeResource(hglobal);
            } else {
                LogIt::Error("Can not load resource defined by name \"{0}\".", $name);
            }
        } else {
            LogIt::Error("Can not find resource with name \"{0}\".", $name);
        }
#else
        using Io::Oidis::XCppCommons::EnvironmentArgs;
        using Io::Oidis::XCppCommons::Primitives::String;

        Elf *elfFile;
        Elf_Scn *section;
        Elf_Data *elfData;
        GElf_Shdr shdr;
        size_t shstrndx;
        char *sectionName;

        string exeName = EnvironmentArgs::getInstance().getExecutableName();

        int fd = open(exeName.c_str(), O_RDONLY, 0);
        if (fd < 0) {
            return;
        }

        if (elf_version(EV_CURRENT) == EV_NONE) {
            return;
        }

        elfFile = elf_begin(fd, ELF_C_READ, nullptr);
        if (elfFile == nullptr) {
            return;
        }

        if (elf_kind(elfFile) != ELF_K_ELF) {
            return;
        }

        if (elf_getshdrstrndx(elfFile, &shstrndx) != 0) {
            return;
        }

        section = nullptr;
        while ((section = elf_nextscn(elfFile, section)) != nullptr) {
            if (gelf_getshdr(section, &shdr) != &shdr) {
                return;
            }

            if ((sectionName = elf_strptr(elfFile, shstrndx, shdr.sh_name)) == nullptr) {
                return;
            }

            if (String::IsEqual(string(sectionName), string(".RESOURCES/") + $name)) {
                elfData = nullptr;
                elfData = elf_getdata(section, elfData);
                if (elfData != nullptr) {
                    $handler(static_cast<char *>(elfData->d_buf), elfData->d_size);
                    break;
                }
            }
        }

        elf_end(elfFile);
        close(fd);
#endif
    }

    void ResourcesManager::setModulePath(const string &$path) {
        if (FileSystem::Exists($path)) {
            LogIt::Warning("Loading resources from different assembly.");
            Utils::modulePath = $path;
        } else {
            LogIt::Error("Trying to set different module for resources manager which could not be found.");
        }
    }

    void *ResourcesManager::getModule() {
        if (!Utils::modulePath.empty()) {
            LogIt::Info("Loading module: {0}", Utils::modulePath);
#ifdef WIN_PLATFORM
            HMODULE hModule = LoadLibraryEx(FileSystem::NormalizePath(Utils::modulePath).c_str(), nullptr,
                                            DONT_RESOLVE_DLL_REFERENCES | LOAD_LIBRARY_AS_DATAFILE);

            if (hModule == nullptr) {
                LogIt::Error("Load of external module {0} failed with code: {1}", Utils::modulePath, GetLastError());
            }
            return hModule;
#endif
        }
        return nullptr;
    }
}
