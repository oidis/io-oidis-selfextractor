/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_HPP_  // NOLINT
#define IO_OIDIS_SELFEXTRACTOR_HPP_

#include <boost/asio.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#ifdef WIN_PLATFORM

#include <objidl.h>
#include <gdiplus.h>

#endif

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"
#include "interfacesMap.hpp"
#include "Io/Oidis/SelfExtractor/sourceFilesMap.hpp"

#include "Io/Oidis/SelfExtractor/Structures/Position.hpp"
#include "Io/Oidis/SelfExtractor/Structures/Size.hpp"
#include "Io/Oidis/SelfExtractor/Structures/ResourceInfo.hpp"

// generated-code-start
#include "Io/Oidis/SelfExtractor/Application.hpp"
#include "Io/Oidis/SelfExtractor/Enums/TextJustifyType.hpp"
#include "Io/Oidis/SelfExtractor/Primitives/BaseGuiObject.hpp"
#include "Io/Oidis/SelfExtractor/ResponseApi/Handlers/StatusResponse.hpp"
#include "Io/Oidis/SelfExtractor/Structures/CacheEntry.hpp"
#include "Io/Oidis/SelfExtractor/Structures/Color.hpp"
#include "Io/Oidis/SelfExtractor/Structures/Font.hpp"
#include "Io/Oidis/SelfExtractor/Structures/SelfExtractorArgs.hpp"
#include "Io/Oidis/SelfExtractor/UserControls/Image.hpp"
#include "Io/Oidis/SelfExtractor/UserControls/Label.hpp"
#include "Io/Oidis/SelfExtractor/UserControls/ProgressBar.hpp"
#include "Io/Oidis/SelfExtractor/UserControls/SplashScreen.hpp"
#include "Io/Oidis/SelfExtractor/Utils/CacheManager.hpp"
#include "Io/Oidis/SelfExtractor/Utils/JSON.hpp"
#include "Io/Oidis/SelfExtractor/Utils/Localization.hpp"
#include "Io/Oidis/SelfExtractor/Utils/ResourcesManager.hpp"
#include "Io/Oidis/SelfExtractor/Utils/ResourcesUpdater.hpp"
#include "Io/Oidis/SelfExtractor/Utils/Time.hpp"
// generated-code-end

#include "Io/Oidis/SelfExtractor/Loader.hpp"

#endif  // IO_OIDIS_SELFEXTRACTOR_HPP_  NOLINT
