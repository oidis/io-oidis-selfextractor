/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_RESOURCEINFO_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_RESOURCEINFO_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * ResourceInfo defines resource element properties.
     * Some properties are used only for ONLINE resource type and some only for RESOURCES (means embedded).
     */
    class ResourceInfo : public Io::Oidis::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Constructs default resource info.
         */
        ResourceInfo();

        /**
         * Constructs resource info from JSON data.
         * @param $data Specify JSON data to be constructed from.
         */
        explicit ResourceInfo(const json &$data);

        /**
         * @return Returns resource name.
         */
        const string &getName() const;

        /**
         * @param $name Specify resource name.
         */
        void setName(const string &$name);

        /**
         * @return Returns resource location.
         */
        const string &getLocation() const;

        /**
         * @param $location Specify resource location.
         */
        void setLocation(const string &$location);

        /**
         * @return Returns true if copy only is required for resource, false for resources which should be unpacked.
         */
        bool isCopyOnly() const;

        /**
         * @param $copyOnly Specify true if only copy operation is required for resource, or false to unpack it.
         */
        void setCopyOnly(bool $copyOnly);

        /**
         * @return Returns resource type. (ONLINE or RESOURCES)
         */
        const string &getType() const;

        /**
         * @param $type Specify resource type. Currently supported are [ONLINE, RESOURCES]
         */
        void setType(const string &$type);

        /**
         * @return Returns project name.
         */
        const string &getProjectName() const;

        /**
         * @param $projectName Specify project name for online resource.
         */
        void setProjectName(const string &$projectName);

        /**
         * @return Returns release name.
         */
        const string &getReleaseName() const;

        /**
         * @param $releaseName Specify release name for online resource.
         */
        void setReleaseName(const string &$releaseName);

        /**
         * @return Returns platform.
         */
        const string &getPlatform() const;

        /**
         * @param $platform Specify release platform for online resource.
         */
        void setPlatform(const string &$platform);

        /**
         * @return Returns version.
         */
        const string &getVersion() const;

        /**
         * @param $version Specify version of online resource.
         */
        void setVersion(const string &$version);

        string ToString() const override;

        friend std::ostream &operator<<(std::ostream &$os, const ResourceInfo &$info);

     private:
        string name = "";
        string location = "";
        bool copyOnly = false;
        string type = "";
        string projectName = "";
        string releaseName = "";
        string platform = "";
        string version = "";
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_RESOURCEINFO_HPP_
