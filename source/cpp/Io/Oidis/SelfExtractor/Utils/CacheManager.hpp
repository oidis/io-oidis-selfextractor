/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_UTILS_CACHEMANAGER_HPP_
#define IO_OIDIS_SELFEXTRACTOR_UTILS_CACHEMANAGER_HPP_

namespace Io::Oidis::SelfExtractor::Utils {
    /**
     * CacheManager class holds singleton of cache entries and mechanism to load them from storage and also save them to storage.
     */
    class CacheManager {
     public:
        /**
         * @return Returns singleton instance of CacheManager.
         */
        static CacheManager &getInstance();

        /**
         * Constructs CacheManager from file.
         * @param $filePath Specify cache file.
         */
        void Init(const string &$filePath);

        /**
         * Save cache content to file.
         */
        void Save();

        /**
         * @return Returns each available cache entries.
         */
        std::vector<Structures::CacheEntry> &getCache() const;

        /**
         * @return Returns path for cache storage file.
         */
        const string &getCacheFilePath() const;

        /**
         * @param $cacheFilePath Specify path to cache storage file.
         */
        void setCacheFilePath(const string &$cacheFilePath);

        /**
         * @param $key Specify cache entry key.
         * @return Returns cache entry specified by key.
         */
        Io::Oidis::SelfExtractor::Structures::CacheEntry getEntry(const string &$key);

        /**
         * Emplaces cache entry into cache content or replace existing one.
         * @param $entry Specify cache entry to be entered or replaced.
         */
        void Emplace(const Io::Oidis::SelfExtractor::Structures::CacheEntry &$entry);

        /**
         * @return Returns true if cache is disabled, false otherwise.
         */
        bool isDisabled() const;

        /**
         * @param $disabled Specify true to disable cache or false otherwise.
         */
        void setDisabled(bool $disabled);

     private:
        CacheManager() = default;

        ~CacheManager() = default;

        mutable std::vector<Io::Oidis::SelfExtractor::Structures::CacheEntry> cache = {};
        string cacheFilePath = "";
        bool disabled = false;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_UTILS_CACHEMANAGER_HPP_
