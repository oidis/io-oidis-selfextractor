/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
    using Io::Oidis::SelfExtractor::Primitives::BaseGuiObject;
    using Io::Oidis::SelfExtractor::Enums::TextJustifyType;

    Label::Label() {
    }

    const TextJustifyType &Label::getJustifyType() const {
        return this->justifyType;
    }

    void Label::setJustifyType(const TextJustifyType &$justifyType) {
        this->justifyType = $justifyType;
    }

    const string &Label::getText() const {
        return this->text;
    }

    void Label::setText(const string &$text) {
        this->text = $text;
    }

#ifdef WIN_PLATFORM

    void Label::Draw(Gdiplus::Graphics &$graphics) {
        if (this->isVisible()) {
            Gdiplus::SolidBrush brush(this->font.getColor().getColor());
            std::wstring wtext(this->text.begin(), this->text.end());

            Gdiplus::RectF rect(this->getPosition().getX(), this->getPosition().getY(), this->getSize().getWidth(),
                                this->getSize().getHeight());

            Gdiplus::StringFormat stringFormat;
            if (this->justifyType == TextJustifyType::CENTER) {
                stringFormat.SetAlignment(Gdiplus::StringAlignment::StringAlignmentCenter);
            } else if (this->justifyType == TextJustifyType::RIGHT) {
                stringFormat.SetAlignment(Gdiplus::StringAlignment::StringAlignmentFar);
            } else {
                stringFormat.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
            }
            stringFormat.SetLineAlignment(stringFormat.GetAlignment());

            $graphics.DrawString(wtext.c_str(), wtext.size(), this->font.getFont(), rect, &stringFormat, &brush);
        }
    }

#endif

    const Structures::Font &Label::getFont() const {
        return this->font;
    }

    void Label::setFont(const Structures::Font &$font) {
        this->font = $font;
    }

    Label::Label(json const &$config)
            : BaseGuiObject($config) {
        if (!$config.empty() && $config.is_object()) {
            this->text = $config.value("text", this->text);
            this->justifyType = TextJustifyType::fromString($config.value("justify", this->justifyType.toString()));
            if ($config.find("font") != $config.end()) {
                this->font = Structures::Font($config["font"]);
            }
        }
    }
}
