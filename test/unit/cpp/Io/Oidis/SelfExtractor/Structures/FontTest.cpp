/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(FontTest, Construction) {
        json data = {
                {"name",  "Courier New"},
                {"size",  14},
                {"bold",  false},
                {"color", "#ff0000"}
        };

        Font font(data);
        ASSERT_STREQ("Courier New", font.getName().c_str());
        ASSERT_EQ(14, font.getSize());
        ASSERT_FALSE(font.isBold());
        ASSERT_STREQ(Color(0xff, 0x00, 0x00).ToString().c_str(), font.getColor().ToString().c_str());

        data = {
                {"name", ""}
        };
        Font font2(data);
    }

    TEST(FontTest, Properties) {
        Font font;

        font.setName("Arial");
        ASSERT_STREQ("Arial", font.getName().c_str());
        font.setSize(11);
        ASSERT_EQ(11, font.getSize());
        font.setBold(true);
        ASSERT_TRUE(font.isBold());
        font.setColor(Color(0x11, 0x22, 0x33, 0x55));
#ifdef WIN_PLATFORM
        ASSERT_STREQ("#55112233", font.getColor().ToString().c_str());
#endif
    }
}  // namespace Io::Oidis::SelfExtractor::Structures
