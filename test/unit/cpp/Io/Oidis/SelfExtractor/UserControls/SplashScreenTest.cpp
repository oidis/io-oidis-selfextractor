/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
    TEST(SplashScreenTest, Construction) {
        json resources = {
                {
                        {"location", "resource1"},
                        {"copyOnly", true}
                },
                {
                        {"location", "somewhere"},
                        {"copyOnly", false}
                }
        };

        std::vector<string> exeArgs = {"--arg1", "--arg2"};

        json data = {
                {"width",          900},
                {"height",         600},
                {"background",     "<path to bg>"},
                {"installPath",    "install"},
                {"executable",     "executable"},
                {"executableArgs", exeArgs},
                {"userControls",
                                   {
                                           {"label",
                                                   {
                                                           {"text", "Some test string"},
                                                           {"x", 111},
                                                           {"y", 123},
                                                           {"width", 200},
                                                           {"height", 35},
                                                           {"justify", "LEFT"},
                                                           {"visible", true},
                                                           {"font",
                                                                   {
                                                                           {"name", "Courier New"},
                                                                           {"size", 14},
                                                                           {"bold", false},
                                                                           {"color", "#ff0000"}
                                                                   }}
                                                   }},
                                           {"labelStatic",
                                                   {
                                                           {"text", "Static string"},
                                                           {"x", 221},
                                                           {"y", 133},
                                                           {"width", 150},
                                                           {"height", 12},
                                                           {"justify", "CENTER"},
                                                           {"visible", true},
                                                           {"font",
                                                                   {
                                                                           {"name", "Arial"},
                                                                           {"size", 21},
                                                                           {"bold", true},
                                                                           {"color", "#a000ffff"}
                                                                   }}
                                                   }},
                                           {"progress",
                                                   {
                                                           {"background", "#ffff00"},
                                                           {"x", 10},
                                                           {"y", 11},
                                                           {"width", 400},
                                                           {"height", 25},
                                                           {"marquee", true},
                                                           {"visible", true},
                                                           {"foreground", "<path to progress>"}
                                                   }}
                                   }
                },
                {"resources",      resources}
        };

        SplashScreen splash(data);
        ASSERT_EQ(900, splash.getSize().getWidth());
        ASSERT_EQ(600, splash.getSize().getHeight());
        ASSERT_STREQ("<path to bg>", splash.getBackground().c_str());
        ASSERT_TRUE(splash.isVisible());
        ASSERT_STREQ("install", splash.getInstallPath().c_str());
        ASSERT_STREQ("executable", splash.getExecutable().c_str());
        ASSERT_STREQ("--arg1", splash.getExecutableArgs()[0].c_str());
        ASSERT_STREQ("--arg2", splash.getExecutableArgs()[1].c_str());

        ASSERT_NE(nullptr, splash.getLabel());
        ASSERT_STREQ("Some test string", splash.getLabel()->getText().c_str());
        ASSERT_EQ(111, splash.getLabel()->getPosition().getX());
        ASSERT_EQ(123, splash.getLabel()->getPosition().getY());
        ASSERT_EQ(200, splash.getLabel()->getSize().getWidth());
        ASSERT_EQ(35, splash.getLabel()->getSize().getHeight());
        ASSERT_EQ(Enums::TextJustifyType::LEFT, splash.getLabel()->getJustifyType());
        ASSERT_TRUE(splash.getLabel()->isVisible());
        ASSERT_STREQ("Courier New", splash.getLabel()->getFont().getName().c_str());
        ASSERT_EQ(14, splash.getLabel()->getFont().getSize());
        ASSERT_FALSE(splash.getLabel()->getFont().isBold());
#ifdef WIN_PLATFORM
        ASSERT_STREQ("#ffff0000", splash.getLabel()->getFont().getColor().ToString().c_str());
#endif
        ASSERT_NE(nullptr, splash.getLabelStatic());
        ASSERT_STREQ("Static string", splash.getLabelStatic()->getText().c_str());
        ASSERT_EQ(221, splash.getLabelStatic()->getPosition().getX());
        ASSERT_EQ(133, splash.getLabelStatic()->getPosition().getY());
        ASSERT_EQ(150, splash.getLabelStatic()->getSize().getWidth());
        ASSERT_EQ(12, splash.getLabelStatic()->getSize().getHeight());
        ASSERT_EQ(Enums::TextJustifyType::CENTER, splash.getLabelStatic()->getJustifyType());
        ASSERT_TRUE(splash.getLabelStatic()->isVisible());
        ASSERT_STREQ("Arial", splash.getLabelStatic()->getFont().getName().c_str());
        ASSERT_EQ(21, splash.getLabelStatic()->getFont().getSize());
        ASSERT_TRUE(splash.getLabelStatic()->getFont().isBold());
#ifdef WIN_PLATFORM
        ASSERT_STREQ("#a000ffff", splash.getLabelStatic()->getFont().getColor().ToString().c_str());
#endif

        ASSERT_NE(nullptr, splash.getProgressBar());
        ASSERT_STREQ("#ffff00", splash.getProgressBar()->getBackground().c_str());
        ASSERT_EQ(10, splash.getProgressBar()->getPosition().getX());
        ASSERT_EQ(11, splash.getProgressBar()->getPosition().getY());
        ASSERT_EQ(400, splash.getProgressBar()->getSize().getWidth());
        ASSERT_EQ(25, splash.getProgressBar()->getSize().getHeight());
        ASSERT_TRUE(splash.getProgressBar()->isVisible());
        ASSERT_TRUE(splash.getProgressBar()->isMarquee());
        ASSERT_STREQ("0x00ffffff", splash.getProgressBar()->getForeground().c_str());

        ASSERT_EQ(2u, splash.getResources().size());
        ASSERT_STREQ("resource1", splash.getResources()[0].getLocation().c_str());
        ASSERT_TRUE(splash.getResources()[0].isCopyOnly());

        ASSERT_STREQ("somewhere", splash.getResources()[1].getLocation().c_str());
        ASSERT_FALSE(splash.getResources()[1].isCopyOnly());
    }
}  // namespace Io::Oidis::SelfExtractor::UserControls
