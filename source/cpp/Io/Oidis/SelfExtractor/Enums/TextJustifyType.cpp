/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Enums {
    using Io::Oidis::XCppCommons::Primitives::BaseEnum;

    WUI_ENUM_IMPLEMENT(TextJustifyType);

    WUI_ENUM_CONST_IMPLEMENT(TextJustifyType, LEFT);
    WUI_ENUM_CONST_IMPLEMENT(TextJustifyType, CENTER);
    WUI_ENUM_CONST_IMPLEMENT(TextJustifyType, RIGHT);
}
