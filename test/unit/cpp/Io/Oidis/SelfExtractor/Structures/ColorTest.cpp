/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(ColorTest, Construction) {
#ifdef WIN_PLATFORM
        Color color("#554433");
        ASSERT_EQ(0x55, color.getColor().GetRed());
        ASSERT_EQ(0x44, color.getColor().GetG());
        ASSERT_EQ(0x33, color.getColor().GetB());
        ASSERT_EQ(0xff, color.getColor().GetA());

        color = Color("0xaabbccdd");
        ASSERT_EQ(0xaa, color.getColor().GetA());
        ASSERT_EQ(0xbb, color.getColor().GetR());
        ASSERT_EQ(0xcc, color.getColor().GetG());
        ASSERT_EQ(0xdd, color.getColor().GetB());

        ASSERT_STREQ("#aabbccdd", color.ToString().c_str());
#endif
    }

    TEST(ColorTest, ToString) {
#ifdef WIN_PLATFORM
        Color color(1, 2, 3);
        ASSERT_STREQ("ff010203", color.ToString("").c_str());
        ASSERT_STREQ("0xffffffff", Color("#ffffff").ToString("0x").c_str());
#endif
    }

    TEST(ColorTest, FromString) {
#ifdef WIN_PLATFORM
        Color color;
        color.FromString("#ffffffff");
        ASSERT_EQ(Gdiplus::Color(0xff, 0xff, 0xff, 0xff).GetValue(), color.getColor().GetValue());
        ASSERT_ANY_THROW(color.FromString("UNKNOWN"));
#endif
    }
}  // namespace Io::Oidis::SelfExtractor::Structures
