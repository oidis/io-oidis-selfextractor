/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Primitives {
    using Io::Oidis::SelfExtractor::Structures::Size;
    using Io::Oidis::SelfExtractor::Structures::Position;

    BaseGuiObject::BaseGuiObject() {}

    BaseGuiObject::BaseGuiObject(const json &$config) {
        if (!$config.empty() && $config.is_object()) {
            this->size.setWidth($config.value("width", this->size.getWidth()));
            this->size.setHeight($config.value("height", this->size.getHeight()));

            this->position.setX($config.value("x", this->position.getX()));
            this->position.setY($config.value("y", this->position.getY()));

            this->visible = $config.value("visible", this->visible);
        }
    }

    const Size &BaseGuiObject::getSize() const {
        return this->size;
    }

    void BaseGuiObject::setSize(const Size &$size) {
        this->size = $size;
    }

    const Position &BaseGuiObject::getPosition() const {
        return this->position;
    }

    void BaseGuiObject::setPosition(const Position &$position) {
        this->position = $position;
    }

    bool BaseGuiObject::isVisible() const {
        return this->visible;
    }

    void BaseGuiObject::setVisible(bool $visible) {
        this->visible = $visible;
    }
}
