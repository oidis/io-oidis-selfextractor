/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_LABEL_HPP_
#define IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_LABEL_HPP_

namespace Io::Oidis::SelfExtractor::UserControls {
    /**
     * Label control displays read-only text value.
     */
    class Label : public Io::Oidis::SelfExtractor::Primitives::BaseGuiObject {
     public:
        /**
         * Constructs default label.
         */
        Label();

        /**
         * Constructs label from JSON configuration.
         * @param $config
         */
        explicit Label(json const &$config);

        /**
         * @return Returns text justification type.
         */
        const Enums::TextJustifyType &getJustifyType() const;

        /**
         * @param $justifyType Specify text justification type.
         */
        void setJustifyType(const Enums::TextJustifyType &$justifyType);

        /**
         * @return Returns label text.
         */
        const string &getText() const;

        /**
         * @param $text Specify label text.
         */
        void setText(const string &$text);

        /**
         * @return Returns font of label's text.
         */
        const Structures::Font &getFont() const;

        /**
         * @param $font Specify font for label's text.
         */
        void setFont(const Structures::Font &$font);

#ifdef WIN_PLATFORM

        void Draw(Gdiplus::Graphics &$graphics) override;

#endif

     private:
        Io::Oidis::SelfExtractor::Enums::TextJustifyType justifyType =
                Io::Oidis::SelfExtractor::Enums::TextJustifyType::LEFT;
        Io::Oidis::SelfExtractor::Structures::Font font{string("Calibri")};
        string text = "";
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_LABEL_HPP_
