/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(PositionTest, Construction) {
        Position position;
        ASSERT_EQ(0, position.getX());
        ASSERT_EQ(0, position.getY());

        position = Position(11, 13);
        ASSERT_EQ(11, position.getX());
        ASSERT_EQ(13, position.getY());

        Position position1(position);
        ASSERT_EQ(11, position1.getX());
        ASSERT_EQ(13, position1.getY());
    }

    TEST(PositionTest, Comparers) {
        Position posA = {15, 16};
        Position posB = {11, 13};
        ASSERT_TRUE(posA > posB);
        ASSERT_FALSE(posA < posB);
        ASSERT_TRUE(posA != posB);
        posA = posB;
        ASSERT_TRUE(posA >= posB);
        ASSERT_TRUE(posA <= posB);
        ASSERT_TRUE(posA == posB);
    }

    TEST(PositionTest, Output) {
        std::ostringstream oss;
        Position position{25, 33};
        oss << position;
        ASSERT_STREQ("[x, y]: [25, 33]", oss.str().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::Structures
