/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_
#define IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_

namespace Io::Oidis::SelfExtractor::Structures {
    /**
     * SelfExtractorArgs class provides program options used in WuiSelfExtractor application.
     */
    class SelfExtractorArgs
            : public Io::Oidis::XCppCommons::Structures::ProgramArgs {
     public:
        /**
         * Constructs args instance with configuration of all program options.
         */
        SelfExtractorArgs();

        /**
         * @return Returns target application path.
         */
        const string &getTarget() const;

        /**
         * @param $value Specify target application path.
         */
        void setTarget(const string &$value);

        /**
         * @return Returns true if configuration print is required, false otherwise.
         */
        bool isPrintConfiguration() const;

        /**
         * @param $printConfiguration Set true to print embedded configuration, false do not print.
         */
        void setPrintConfiguration(bool $printConfiguration);

        /**
         * @return Returns true if cache should be disabled and all resources and configs will be downloaded.
         */
        bool isDisableCache() const;

        /**
         * @param $disableCache Set true to disable cache, false otherwise.
         */
        void setDisableCache(bool $disableCache);

        /**
         * @return Returns true if headless mode is required (SelfExtractor without GUI)
         */
        bool isHeadless() const;

        /**
         * @param $headless Set true to enable headless mode (SelfExtractor without GUI)
         */
        void setHeadless(bool $headless);

        /**
         * @return Returns arguments string for application. Preconfigured executable will be invoked with '--appArgs="<CLI args>"'.
         */
        const string &getAppArgs() const;

        /**
         * @param $appArgs Specify arguments string for application. Preconfigured executable will be invoked with '--appArgs="<CLI args>"'.
         */
        void setAppArgs(const string &$appArgs);

        /**
         * @return Returns true if configured executable execute should be skipped, false otherwise.
         */
        bool isSkipExecute() const;

        /**
         * @param $skipExecute Set true to skip configured executable execute, false otherwise.
         */
        void setSkipExecute(bool $skipExecute);

        /**
         * @return Returns true if selfextractor should print application path to stdOut.
         */
        bool isPrintAppPath() const;

        /**
         * @param $printAppPath Set true to print application path to stdOut, false otherwise.
         */
        void setPrintAppPath(bool $printAppPath);

        /**
         * @return Returns path to external module.
         */
        const string &getExtModule() const;

        /**
         * @param $path Specify path to external module.
         */
        void setExtModule(const string &$path);

        /**
         * @return Returns true if debug mode is required from command line.
         */
        bool isDebug() const;

        /**
         * @param $debug Specify true to require debug mode.
         */
        void setDebug(bool $debug);

        void setCleanupDisabled(bool $value);

        bool isCleanupDisabled() const;

        void setRunOnly(bool $value);

        bool isRunOnly() const;

     private:
        string target;
        bool printConfiguration = false;
        bool disableCache = false;
        bool headless = false;
        string appArgs;
        bool skipExecute = false;
        bool printAppPath = false;
        string extModule;
        bool debug = false;
        bool cleanupDisabled = false;
        bool runOnly = false;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_
