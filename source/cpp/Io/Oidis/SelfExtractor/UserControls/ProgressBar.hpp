/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_PROGRESSBAR_HPP_
#define IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_PROGRESSBAR_HPP_

namespace Io::Oidis::SelfExtractor::UserControls {
    /**
     * ProgressBar class provides GUI controls which shows horizontal progress with support of marquee style.
     */
    class ProgressBar : public Io::Oidis::SelfExtractor::Primitives::BaseGuiObject {
     public:
        /**
         * Constructs default ProgressBar.
         */
        ProgressBar() = default;

        /**
         * Constructs ProgressBar from values.
         * @param $background Specify background image path or color string.
         * @param $foreground Specify foreground image path or color string.
         * @param $marquee Select true to use marquee progress style, false otherwise.
         */
        ProgressBar(const string &$background, const string &$foreground, bool $marquee);

        /**
         * Constructs ProgressBar from JSON configuration.
         * @param $config
         */
        explicit ProgressBar(const json &$config);

        /**
         * @return Returns background image path or color string.
         */
        const string &getBackground() const;

        /**
         * @param $background Specify background image path or color string.
         */
        void setBackground(const string &$background);

        /**
         * @return Returns foreground image path or color string.
         */
        const string &getForeground() const;

        /**
         * @param $foreground Specify foreground image path or color string.
         */
        void setForeground(const string &$foreground);

        /**
         * @return Returns true if marquee progress style is selected, false otherwise.
         */
        bool isMarquee() const;

        /**
         * @param $marquee Specify true to select marquee progress style, false otherwise.
         */
        void setMarquee(bool $marquee);

        /**
         * @return Returns progress minimal value.
         */
        int getMin() const;

        /**
         * @param $min Specify progress minimal value.
         */
        void setMin(int $min);

        /**
         * @return Returns progress maximal value.
         */
        int getMax() const;

        /**
         * @param $max Specify progress maximal value.
         */
        void setMax(int $max);

        /**
         * @return Returns actual progress value.
         */
        int getValue() const;

        /**
         * @param $value Specify actual progress value.
         */
        void setValue(int $value);

        /**
         * @return Returns true if tray-progress is attached to ProgressBar, false otherwise.
         */
        bool isTrayProgress() const;

        /**
         * @param $trayProgress Specify true to attach tray-progress to ProgressBar, false otherwise.
         */
        void setTrayProgress(bool $trayProgress);

#ifdef WIN_PLATFORM

        void Draw(Gdiplus::Graphics &$graphics) override;

#endif

     private:
        string background = "#ffff00";
        string foreground = "#ff00ff";
        shared_ptr<Io::Oidis::SelfExtractor::UserControls::Image> bgImage = nullptr, fgImage = nullptr;
        bool marquee = false;
        bool trayProgress = false;
        int min = 0;
        int max = 100;
        int value = 0;
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_PROGRESSBAR_HPP_
