/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_IMAGE_HPP_
#define IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_IMAGE_HPP_

namespace Io::Oidis::SelfExtractor::UserControls {
    /**
     * Image class defines GUI control which stores and draws images in various formats include GIF.
     */
    class Image : public Io::Oidis::SelfExtractor::Primitives::BaseGuiObject {
     public:
        /**
         * Constructs default image control.
         */
        Image() = default;

        /**
         * Release internal data.
         */
        virtual ~Image();

        /**
         * Load image from file path. Supports each formats supported by Gdiplus::Image.
         * @param $path Specify image file path.
         */
        void Load(const string &$path);

#ifdef WIN_PLATFORM

        void Draw(Gdiplus::Graphics &$graphics) override;

#endif

     private:
#ifdef WIN_PLATFORM
        shared_ptr<Gdiplus::Image> nativeImage = nullptr;
#endif
        bool isGif = false;
        int frameIndex = 0;
        int framesCnt = 1;
        int tick = -1;
        int loopDelay = 0;
        const int nativeTick = 10;  // ms
#ifdef WIN_PLATFORM
        Gdiplus::PropertyItem *propertyItem = nullptr;
#endif
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_USERCONTROLS_IMAGE_HPP_
