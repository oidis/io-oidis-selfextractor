/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Utils {
    using Io::Oidis::XCppCommons::Primitives::String;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::System::Net::Url;
    using Io::Oidis::XCppCommons::Utils::ObjectDecoder;
    using Io::Oidis::XCppCommons::Utils::ObjectEncoder;
    using Io::Oidis::XCppCommons::Utils::Convert;
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::SelfExtractor::Structures::ResourceInfo;
    using Io::Oidis::SelfExtractor::Utils::JSON;
    using Io::Oidis::SelfExtractor::Utils::CacheManager;
    using Io::Oidis::SelfExtractor::Utils::Time;
    using Io::Oidis::SelfExtractor::Structures::CacheEntry;
    using Io::Oidis::SelfExtractor::Application;
    using Io::Oidis::XCppCommons::System::ResponseApi::ResponseFactory;
    using Io::Oidis::SelfExtractor::ResponseApi::Handlers::StatusResponse;

    string getResourceID(const ResourceInfo &$info) {
        string id = "" + $info.getLocation();

        if (!$info.getProjectName().empty()) {
            json keyData = {$info.getProjectName(), $info.getReleaseName(), $info.getPlatform()};
            id = keyData.dump();
        }

        return String::getSha1(id);
    }

    bool ResourcesUpdater::UpdateAvailable(const ResourceInfo &$info, long $timestamp, const string &$version) {
        bool updateAvailable = false;
        string url = $info.getLocation();

        Url urlObj(url);
        if (!urlObj.getProtocol().empty()) {
            json options = {
                    {"url",          url},
                    {"headers",      {
                                             {"charset", "UTF-8"},
                                             {"User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                            "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}
                                     }
                    },
                    {"streamOutput", true}
            };

            if (!$info.getProjectName().empty()) {
                string timestamp = std::to_string($timestamp * 1000LL);
                json jData = {
                        {"data",   ObjectEncoder::Base64(json(
                                {
                                        {"args", ObjectEncoder::Base64(
                                                json({
                                                             $info.getProjectName(),
                                                             $info.getReleaseName(),
                                                             $info.getPlatform(),
                                                             $version,
                                                             timestamp
                                                     }).dump())},
                                        {"name", "Io.Oidis.Hub.Utils.SelfupdatesManager.UpdateExists"}
                                }).dump())
                        },
                        {"id",     Time::getTimestamp() * 1000LL},
                        {"origin", "http://localhost"},
                        {"type",   "LiveContentWrapper.InvokeMethod"},
                        {"token",  ""}
                };

                options["method"] = "POST";
                options["body"] = "jsonData=" + jData.dump();
                options["headers"]["content-type"] = "application/x-www-form-urlencoded; charset=UTF-8";

                FileSystem::Download(options, [&](const string &$data) {
                    LogIt::Info(R"(Get data "{0}" from "{1}")", $data, url);
                    json lcData = json::parse($data);
                    if (!lcData.empty() && lcData.is_object()) {
                        if (lcData.value("status", 0) == 200) {
                            string val = ObjectDecoder::Base64(lcData["data"]["returnValue"]);
                            if (boost::iequals("true", boost::to_lower_copy(boost::trim_copy(val)))) {
                                updateAvailable = true;
                            }
                        }
                    }
                });
            } else {
                options["method"] = "HEAD";
                long timestamp = 0;

                StatusResponse statusResponse;

                statusResponse.setOnComplete([&](const json &$headers, const string &$data) {
                    timestamp = Convert::LastModifiedToTimestamp(this->findHeaderKeyIgnoreCase($headers, "last-modified"));

                    if (timestamp > $timestamp) {
                        updateAvailable = true;
                    }

                    LogIt::Info(R"(Get headers for "{0}" from "{1}")", $headers, url);
                });

                FileSystem::Download(options, ResponseFactory::getResponse(statusResponse));
            }
        }
        return updateAvailable;
    }

    string ResourcesUpdater::DownloadResource(const string &$url, const ResourceInfo &$info,
                                              const function<void(const json $data)> &$handler) {
        string resPath = $url;
        Url url($url);
        ResourceInfo info = $info;

        if (!url.getProtocol().empty()) {
            json options = {
                    {"url",          $url},
                    {"headers",      {
                                             {"charset", "UTF-8"},
                                             {"User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                            "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}
                                     }
                    },
                    {"streamOutput", false}
            };

            string fileName;
            if (!info.getProjectName().empty()) {
                json jData = {
                        {"data",   ObjectEncoder::Base64(json(
                                {
                                        {"args", ObjectEncoder::Base64(
                                                json({
                                                             info.getProjectName(),
                                                             info.getReleaseName(),
                                                             info.getPlatform()
                                                     }).dump())},
                                        {"name", "Io.Oidis.Hub.Utils.SelfupdatesManager.DownloadRaw"}
                                }).dump())
                        },
                        {"id",     Time::getTimestamp() * 1000LL},
                        {"origin", "http://localhost"},
                        {"type",   "LiveContentWrapper.InvokeMethod"},
                        {"token",  ""}
                };

                options["method"] = "POST";
                options["body"] = "jsonData=" + jData.dump();
                options["headers"]["content-type"] = "application/x-www-form-urlencoded; charset=UTF-8";
            } else {
                options["method"] = "GET";
                info.setLocation($url);
            }

            bool skipDownload = false;

            if (!CacheManager::getInstance().isDisabled()) {
                auto cdata = CacheManager::getInstance().getEntry(getResourceID(info));
                if (!cdata.Empty()) {
                    json objData = json::parse(ObjectDecoder::Base64(cdata.getData()));
                    if (!UpdateAvailable(info, cdata.getTimestamp(), objData.value("version", ""))) {
                        resPath = objData.value("path", "");
                        if (FileSystem::Exists(resPath)) {
                            LogIt::Info("Resource file already exists at \"{0}\"", resPath);
                            skipDownload = true;
                        } else {
                            LogIt::Warning("Cache entry contains path to not existing file \"{0}\". Will try download.", resPath);
                        }
                    } else {
                        LogIt::Info("Found new version for \n{0}, timestamp: {1}. Will be downloaded.", info, cdata.getTimestamp());
                    }
                }
            }

            if (!skipDownload) {
                auto parseVersion = [](const string &$file) -> string {
                    string retVal;
                    boost::regex ex(R"((\d+-\d+-\d+(?:-[a-zA-Z]+)?))");
                    boost::match_results<std::string::const_iterator> what;
                    if (boost::regex_search($file.begin(), $file.end(), what, ex, boost::match_default)) {
                        retVal = what[1].str();
                    }
                    return retVal;
                };

                auto repareVersionFormat = [](const string &$version) -> string {
                    string retVal = $version;
                    boost::regex ex(R"((\d(?:-\d)+))");
                    boost::match_results<std::string::const_iterator> what;
                    if (boost::regex_search($version.begin(), $version.end(), what, ex, boost::match_default)) {
                        retVal = String::Replace($version, what.str(), String::Replace(what.str(), "-", "."));
                    }
                    return retVal;
                };

                auto cdata = CacheManager::getInstance().getEntry(getResourceID(info));
                if (!cdata.Empty()) {
                    resPath = json::parse(ObjectDecoder::Base64(cdata.getData()))["path"];
                    LogIt::Info("Old resource file found at \"{0}\", will be deleted", resPath);
                    FileSystem::Delete(resPath);
                }

                int timestamp = 0;
                string version;

                StatusResponse statusResponse{};

                statusResponse.setOnComplete([&](const json &$headers, const string &$data) {
                    timestamp = Convert::LastModifiedToTimestamp(this->findHeaderKeyIgnoreCase($headers, "last-modified"));

                    LogIt::Info(R"(Downloaded "{0}" from "{1}" with headers {2})", $data, $url, $headers);
                    resPath = $data;
                    version = parseVersion($data);

                    if ($handler != nullptr) {
                        json data = {{"type", "oncomplete"},
                                     {"data", $data}};
                        $handler(data);
                    }
                });

                statusResponse.setOnChange([&](const json &$data) {
                    if ($handler != nullptr) {
                        $handler($data);
                    }
                });

                FileSystem::Download(options, ResponseFactory::getResponse(statusResponse));

                string newFilePath = Application::getAppDataPath() + "/" + boost::filesystem::path(resPath).filename().string();
                FileSystem::Copy(resPath, newFilePath, [&](bool status) {
                    if (status) {
                        FileSystem::Delete(resPath);
                        resPath = newFilePath;
                    } else {
                        LogIt::Warning(R"(Resource file cannot be moved from "{0}" to "{1}".)", resPath, newFilePath);
                    }
                });

                if (!resPath.empty() && FileSystem::Exists(resPath)) {
                    json tmpInfo = {
                            {"path",        resPath},
                            {"version",     repareVersionFormat(version)},
                            {"releaseName", info.getReleaseName()},
                            {"projectName", info.getProjectName()},
                            {"platform",    info.getPlatform()}
                    };
                    CacheManager::getInstance().Emplace(CacheEntry(getResourceID(info), ObjectEncoder::Base64(tmpInfo.dump()), timestamp));
                } else {
                    LogIt::Warning("File \"{0}\" not exists or has not been downloaded. No cache entry is created.", resPath);
                }
            }
        }
        return resPath;
    }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"

    void ResourcesUpdater::DownloadSingleConfig(const json &$options, const function<void(const json &$data)> &$handler) {
        FileSystem::Download(json($options), [&](const string &$data) {
            if (!$data.empty()) {
                json cfg = JSON::ParseJsonp($data);
                LogIt::Debug("Downloaded config:\n{0}", cfg);
                if (!cfg.empty() && cfg.is_object()) {
                    if ($handler) {
                        $handler(cfg);
                    }
                    if (cfg.find("extendsConfig") != cfg.end()) {
                        json options = $options;
                        string url = cfg.value("extendsConfig", "");
                        if (!url.empty()) {
                            options["url"] = url;
                            ResourcesUpdater::DownloadSingleConfig(json(options), $handler);
                        }
                    }
                } else {
                    LogIt::Error("Received configuration is not valid JSONP.\n{0}", $data);
                }
            }
        });
    }

#pragma clang diagnostic pop

    json ResourcesUpdater::DownloadConfig(const json &$options, const json &$config) {
        json tmpData = $config;

        std::vector<json> configs;
        ResourcesUpdater::DownloadSingleConfig($options, [&](const json &$cfg) {
            configs.push_back($cfg);
        });

        // merge them all
        for (auto rit = configs.rbegin(); rit != configs.rend(); rit++) {
            tmpData = JSON::CombineJson(tmpData, *rit);
        }

        return tmpData;
    }

    string ResourcesUpdater::findHeaderKeyIgnoreCase(const json &$obj, const string &$key) {
        string retVal;
        for (json::const_iterator it = $obj.begin(); it != $obj.end(); ++it) {
            if (String::ToLowerCase(it.key()) == String::ToLowerCase($key)) {
                retVal = it.value();
                break;
            }
        }
        return retVal;
    }
}
