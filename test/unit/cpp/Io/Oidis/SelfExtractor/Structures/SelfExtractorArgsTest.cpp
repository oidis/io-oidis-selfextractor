/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Utils::ArgsParser;
namespace po = boost::program_options;

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(SelfExtractorArgsTest, getTarget) {
        SelfExtractorArgs selfExtractorArgs;

        ASSERT_STREQ("", selfExtractorArgs.getTarget().c_str());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--target=resources/target")};
        ArgsParser::Parse(selfExtractorArgs, 2, (const char **) argv);
        ASSERT_STREQ("resources/target", selfExtractorArgs.getTarget().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::Structures
