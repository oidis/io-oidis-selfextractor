/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    namespace po = boost::program_options;

    SelfExtractorArgs::SelfExtractorArgs() {
        po::options_description appDescription("SelfExtractor options");

        appDescription.add_options()
                ("target", po::value<string>()->notifier(boost::bind(&SelfExtractorArgs::setTarget, this, _1)),
                 "Specify target application path.")
                ("get-config", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setPrintConfiguration, this, _1)),
                 "Prints internal SelfExtractor configuration in JSON format.")
                ("disable-cache", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setDisableCache, this, _1)),
                 "Set true to force download resource files and ignore already cached ones.")
                ("appArgs", po::value<string>()->notifier(boost::bind(&SelfExtractorArgs::setAppArgs, this, _1)),
                 "Specify arguments string for application. Preconfigured executable will be invoked with '--appArgs=\"<CLI args>\"'.")
                ("headless", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setHeadless, this, _1)),
                 "Set true to enable headless mode (SelfExtractor without GUI).")
                ("skipExecute", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setSkipExecute, this, _1)),
                 "Set true to skip execute of configured executable.")
                ("printAppPath", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setPrintAppPath, this, _1)),
                 "Set true if application path should by printed to stdOut.")
                ("extModule", po::value<string>()->notifier(boost::bind(&SelfExtractorArgs::setExtModule, this, _1)),
                 "Specify path to external module (SelfExtractor executable).")
                ("debug", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setDebug, this, _1)),
                 "Set true to force switch to debug mode.")
                ("disable-cleanup", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setCleanupDisabled, this, _1)),
                 "Set true to disable install folder deletion at startup.")
                ("run-only", po::bool_switch()->notifier(boost::bind(&SelfExtractorArgs::setRunOnly, this, _1)),
                 "Set true to disable resources replacement if install folder exists.");

        this->getDescription()->add(appDescription);
    }

    const string &SelfExtractorArgs::getTarget() const {
        return this->target;
    }

    void SelfExtractorArgs::setTarget(const string &$value) {
        this->target = $value;
    }

    bool SelfExtractorArgs::isPrintConfiguration() const {
        return this->printConfiguration;
    }

    void SelfExtractorArgs::setPrintConfiguration(bool $printConfiguration) {
        this->printConfiguration = $printConfiguration;
    }

    bool SelfExtractorArgs::isDisableCache() const {
        return this->disableCache;
    }

    void SelfExtractorArgs::setDisableCache(bool $disableCache) {
        this->disableCache = $disableCache;
    }

    bool SelfExtractorArgs::isHeadless() const {
        return this->headless;
    }

    void SelfExtractorArgs::setHeadless(bool $headless) {
        this->headless = $headless;
    }

    const string &SelfExtractorArgs::getAppArgs() const {
        return this->appArgs;
    }

    void SelfExtractorArgs::setAppArgs(const string &$appArgs) {
        this->appArgs = $appArgs;
    }

    bool SelfExtractorArgs::isSkipExecute() const {
        return this->skipExecute;
    }

    void SelfExtractorArgs::setSkipExecute(bool $skipExecute) {
        this->skipExecute = $skipExecute;
    }

    bool SelfExtractorArgs::isPrintAppPath() const {
        return this->printAppPath;
    }

    void SelfExtractorArgs::setPrintAppPath(bool $printAppPath) {
        this->printAppPath = $printAppPath;
    }

    const string &SelfExtractorArgs::getExtModule() const {
        return this->extModule;
    }

    void SelfExtractorArgs::setExtModule(const string &$path) {
        this->extModule = $path;
    }

    bool SelfExtractorArgs::isDebug() const {
        return this->debug;
    }

    void SelfExtractorArgs::setDebug(bool $debug) {
        this->debug = $debug;
    }

    void SelfExtractorArgs::setCleanupDisabled(bool $value) {
        this->cleanupDisabled = $value;
    }

    bool SelfExtractorArgs::isCleanupDisabled() const {
        return this->cleanupDisabled;
    }

    void SelfExtractorArgs::setRunOnly(bool $value) {
        this->runOnly = $value;
    }

    bool SelfExtractorArgs::isRunOnly() const {
        return this->runOnly;
    }
}
