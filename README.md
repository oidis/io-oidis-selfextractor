# com-wui-framework-selfextractor v2020.2.0

> SelfExtractor for WUI Framework's applications

## Requirements

This application depends on the [WUI Builder](https://gitlab.com/oidis/io-oidis-builder).
See the WUI Builder requirements before you build this project.

## Project Build

This project build is fully automated. For more information about the project build, 
see the [WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html)
from the C++ source by running the `docs` command.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2020.2.0
Updated progress handling in embedded resources mode. Releases configuration migrated to solutions.
### v2019.1.0
Fixed issue with http header key case-sensitivity
### v2018.1.1
Change of selfextractor storage path to local APPDATA.
### v2018.1.0
Usage of internal ProductName from embedded version info instead of module file name. Usage of 7zip and p7zip as dependencies.
### v2018.0.1
Terminate blocking process in output directory before package unpack. Updated configs version to new format.
### v2018.0.0
Added external link library according to XCppCommons update. Added possibility to skip run of executable into configuration and standard
output with final application path. Changed version format.
### v1.2.0
Added some log information. Integrated support for Linux and Mac.
### v1.1.1
Internal configuration file formats migrated from XML to JSONP.
### v1.1.0
Added novel ResponseApi.
### v1.0.1
Added main selfextractor features for online and offline modes. Added online and offline configuration. Added basic GUI components and window management.
### v1.0.0
Initial release

## License

This software is owned or controlled by Oidis.
Use of this software is governed by the BSD-3-Clause License distributed with this material.
 
See the `LICENSE.txt` file distributed for more details.

---

Author Michal Kelnar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)
