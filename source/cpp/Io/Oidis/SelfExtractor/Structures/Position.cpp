/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    Position::Position()
            : x(0),
              y(0) {}

    Position::Position(int $x, int $y)
            : x($x),
              y($y) {}

    int Position::getX() const {
        return this->x;
    }

    void Position::setX(int $x) {
        this->x = $x;
    }

    int Position::getY() const {
        return this->y;
    }

    void Position::setY(int $y) {
        this->y = $y;
    }

    bool Position::operator<(const Position &$rhs) const {
        if (this->x < $rhs.x) {
            return true;
        }
        if ($rhs.x < this->x) {
            return false;
        }
        return this->y < $rhs.y;
    }

    bool Position::operator>(const Position &$rhs) const {
        return $rhs < *this;
    }

    bool Position::operator<=(const Position &$rhs) const {
        return !($rhs < *this);
    }

    bool Position::operator>=(const Position &$rhs) const {
        return !(*this < $rhs);
    }

    bool Position::operator==(const Position &$rhs) const {
        return x == $rhs.x &&
               y == $rhs.y;
    }

    bool Position::operator!=(const Position &$rhs) const {
        return !($rhs == *this);
    }

    std::ostream &operator<<(std::ostream &$os, const Position &$position) {
        $os << "[x, y]: [" << $position.x << ", " << $position.y << "]";
        return $os;
    }
}
