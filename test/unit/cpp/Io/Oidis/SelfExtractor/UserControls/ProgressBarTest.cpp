/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
    TEST(ProgressBarTest, Construction) {
        json data = {
                {"background", "#ffff00"},
                {"x", 10},
                {"y", 20},
                {"width", 400},
                {"height", 25},
                {"marquee", true},
                {"visible", true},
                {"foreground", "<path to foreground>"}
        };

        ProgressBar progressBar(data);
        ASSERT_STREQ("#ffff00", progressBar.getBackground().c_str());
        ASSERT_EQ(10, progressBar.getPosition().getX());
        ASSERT_EQ(20, progressBar.getPosition().getY());
        ASSERT_EQ(400, progressBar.getSize().getWidth());
        ASSERT_EQ(25, progressBar.getSize().getHeight());
        ASSERT_TRUE(progressBar.isMarquee());
        ASSERT_TRUE(progressBar.isVisible());
        ASSERT_STREQ("0x00ffffff", progressBar.getForeground().c_str());
    }
}  // namespace Io::Oidis::SelfExtractor::UserControls
