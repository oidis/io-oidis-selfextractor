/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    Size::Size()
            : width(0),
              height(0) {}

    Size::Size(int $width, int $height)
            : width($width),
              height($height) {}

    bool Size::operator<(const Size &$rhs) const {
        if (this->width < $rhs.width) {
            return true;
        }
        if ($rhs.width < this->width) {
            return false;
        }
        return this->height < $rhs.height;
    }

    bool Size::operator>(const Size &$rhs) const {
        return $rhs < *this;
    }

    bool Size::operator<=(const Size &$rhs) const {
        return !($rhs < *this);
    }

    bool Size::operator>=(const Size &$rhs) const {
        return !(*this < $rhs);
    }

    bool Size::operator==(const Size &$rhs) const {
        return (this->width == $rhs.width) && (this->height == $rhs.height);
    }

    bool Size::operator!=(const Size &$rhs) const {
        return !($rhs == *this);
    }

    std::ostream &operator<<(std::ostream &$os, const Size &$size) {
        $os << "[width, height]: [" << $size.width << ", " << $size.height << "]";
        return $os;
    }

    int Size::getWidth() const {
        return this->width;
    }

    void Size::setWidth(int $width) {
        this->width = $width;
    }

    int Size::getHeight() const {
        return this->height;
    }

    void Size::setHeight(int $height) {
        this->height = $height;
    }
}
