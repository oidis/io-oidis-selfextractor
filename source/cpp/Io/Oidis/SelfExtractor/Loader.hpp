/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_LOADER_HPP_
#define IO_OIDIS_SELFEXTRACTOR_LOADER_HPP_

namespace Io::Oidis::SelfExtractor {
    /**
     * Loader class provides static application methods.
     */
    class Loader : public Io::Oidis::XCppCommons::Loader {
     public:
        /**
         * Contains application logic entry method.
         * @param $argc An argument count.
         * @param $argv An array of arguments.
         * @return Returns exit code.
         */
        static int Load(const int $argc, const char *$argv[]) {
            // Log directory is located in system LOCALAPPDATA/WUIFramework because selfextractor can run from various file system path.
            using Io::Oidis::XCppCommons::Primitives::String;
            using Io::Oidis::XCppCommons::System::IO::FileSystem;
            using Io::Oidis::SelfExtractor::Application;

            boost::filesystem::path basePath = boost::filesystem::path(FileSystem::getLocalAppDataPath()) /
                                               "WUIFramework" /
                                               "com-wui-framework-selfextractor" /
                                               String::Replace(String::Replace(Application::ResolveOriginalFileName(
                                                       boost::filesystem::path($argv[0]).string()), ".", "_"), " ", "_");

            if (!boost::filesystem::exists(basePath)) {
                boost::filesystem::create_directories(basePath);
            }
            auto ioHandler = Io::Oidis::XCppCommons::IOApi::IOHandlerFactory::getHandler(
                    Io::Oidis::XCppCommons::Enums::IOHandlerType::OUTPUT_FILE, "LogIt");
            std::dynamic_pointer_cast<Io::Oidis::XCppCommons::IOApi::Handlers::OutputFileHandler>(ioHandler)->setBasePath(
                    (basePath).string());

            Application application;
            Application::setAppDataPath(basePath.string());
            return Io::Oidis::XCppCommons::Loader::Load(application, $argc, $argv);
        }
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_LOADER_HPP_
