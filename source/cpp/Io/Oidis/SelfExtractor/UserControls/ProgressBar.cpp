/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::UserControls {
    using Io::Oidis::XCppCommons::Primitives::String;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    ProgressBar::ProgressBar(const string &$background, const string &$foreground, bool $marquee)
            : marquee($marquee) {
        this->setBackground($background);
        this->setForeground($foreground);
    }

    const string &ProgressBar::getBackground() const {
        return this->background;
    }

    void ProgressBar::setBackground(const string &$background) {
        this->background = $background;
        if (!String::StartsWith(this->background, "#") && !String::StartsWith(this->background, "0x")) {
            if (FileSystem::Exists(this->background) && !boost::filesystem::is_directory(this->background)) {
                this->bgImage.reset(new Image());
                this->bgImage->Load(this->background);
                this->bgImage->setSize(this->getSize());
                this->bgImage->setPosition(this->getPosition());
                this->bgImage->setVisible(this->isVisible());
            } else {
                LogIt::Warning("Can not locate specified file or \"{0}\" is not file. Using transparent color.", this->background);
                this->background = "0x00ffffff";
            }
        } else {
            this->bgImage = nullptr;
        }
    }

    const string &ProgressBar::getForeground() const {
        return this->foreground;
    }

    void ProgressBar::setForeground(const string &$foreground) {
        this->foreground = $foreground;
        if (!String::StartsWith(this->foreground, "#") && !String::StartsWith(this->foreground, "0x")) {
            if (FileSystem::Exists(this->foreground) && !boost::filesystem::is_directory(this->foreground)) {
                this->fgImage.reset(new Image());
                this->fgImage->Load(this->foreground);
                this->fgImage->setSize(this->getSize());
                this->fgImage->setPosition(this->getPosition());
                this->fgImage->setVisible(this->isVisible());
            } else {
                LogIt::Warning("Can not locate specified file or \"{0}\" is not file. Using transparent color.", this->foreground);
                this->foreground = "0x00ffffff";
            }
        } else {
            this->fgImage = nullptr;
        }
    }

    bool ProgressBar::isMarquee() const {
        return this->marquee;
    }

    void ProgressBar::setMarquee(bool $marquee) {
        this->marquee = $marquee;
    }

#ifdef WIN_PLATFORM

    void ProgressBar::Draw(Gdiplus::Graphics &$graphics) {
        if (this->isVisible()) {
            if (this->bgImage != nullptr) {
                this->bgImage->Draw($graphics);
            } else {
                Gdiplus::SolidBrush brush(Structures::Color(this->background).getColor());
                $graphics.FillRectangle(&brush, this->getPosition().getX(), this->getPosition().getY(), this->getSize().getWidth(),
                                        this->getSize().getHeight());
            }

            int tmpWidth = 0;
            if (this->getMax() > this->getMin() && this->getValue() <= this->getMax() && this->getValue() >= this->getMin()) {
                tmpWidth = static_cast<int>(static_cast<float>(this->getSize().getWidth()) / (this->getMax() - this->getMin()) *
                                            this->getValue());
            }
            if (this->fgImage != nullptr) {
                if (!this->marquee) {
                    this->fgImage->setSize(Structures::Size(tmpWidth, this->fgImage->getSize().getHeight()));
                }
                this->fgImage->Draw($graphics);
            } else {
                Gdiplus::SolidBrush brush(Structures::Color(this->foreground).getColor());
                $graphics.FillRectangle(&brush, this->getPosition().getX(), this->getPosition().getY(), tmpWidth,
                                        this->getSize().getHeight());
            }
        }
    }

#endif

    int ProgressBar::getMin() const {
        return this->min;
    }

    void ProgressBar::setMin(int $min) {
        this->min = $min;
    }

    int ProgressBar::getMax() const {
        return this->max;
    }

    void ProgressBar::setMax(int $max) {
        this->max = $max;
    }

    int ProgressBar::getValue() const {
        return this->value;
    }

    void ProgressBar::setValue(int $value) {
        this->value = $value;
    }

    bool ProgressBar::isTrayProgress() const {
        return this->trayProgress;
    }

    void ProgressBar::setTrayProgress(bool $trayProgress) {
        this->trayProgress = $trayProgress;
    }

    ProgressBar::ProgressBar(json const &$config)
            : BaseGuiObject($config) {
        if (!$config.empty() && $config.is_object()) {
            this->marquee = $config.value("marquee", this->marquee);
            this->setForeground($config.value("foreground", this->foreground));
            this->setBackground($config.value("background", this->background));
            this->setTrayProgress($config.value("trayProgress", this->trayProgress));
        }
    }
}
