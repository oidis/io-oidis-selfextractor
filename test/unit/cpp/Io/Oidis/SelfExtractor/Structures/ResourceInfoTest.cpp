/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::SelfExtractor::Structures {
    TEST(ResourceInfoTest, Construction) {
        json data = {
                {"name",        "<name>"},
                {"location",    "<location>"},
                {"copyOnly",    true},
                {"type",        "ONLINE"},
                {"projectName", "<project-name>"},
                {"releaseName", "<release-name>"},
                {"platform",    "<platform>"},
                {"version",     "1.2.3-zzz"}
        };

        ResourceInfo resourceInfo(data);
        ASSERT_STREQ("<name>", resourceInfo.getName().c_str());
        ASSERT_STREQ("<location>", resourceInfo.getLocation().c_str());
        ASSERT_TRUE(resourceInfo.isCopyOnly());
        ASSERT_STREQ("ONLINE", resourceInfo.getType().c_str());
        ASSERT_STREQ("<project-name>", resourceInfo.getProjectName().c_str());
        ASSERT_STREQ("<release-name>", resourceInfo.getReleaseName().c_str());
        ASSERT_STREQ("<platform>", resourceInfo.getPlatform().c_str());
        ASSERT_STREQ("1.2.3-zzz", resourceInfo.getVersion().c_str());
    }

    TEST(ResourceInfoTest, Properties) {
        ResourceInfo resourceInfo;

        resourceInfo.setName("<name>");
        ASSERT_STREQ("<name>", resourceInfo.getName().c_str());
        resourceInfo.setLocation("<location>");
        ASSERT_STREQ("<location>", resourceInfo.getLocation().c_str());
        resourceInfo.setCopyOnly(true);
        ASSERT_TRUE(resourceInfo.isCopyOnly());
        resourceInfo.setType("ONLINE");
        ASSERT_STREQ("ONLINE", resourceInfo.getType().c_str());
        resourceInfo.setProjectName("<project-name>");
        ASSERT_STREQ("<project-name>", resourceInfo.getProjectName().c_str());
        resourceInfo.setReleaseName("<release-name>");
        ASSERT_STREQ("<release-name>", resourceInfo.getReleaseName().c_str());
        resourceInfo.setPlatform("<platform>");
        ASSERT_STREQ("<platform>", resourceInfo.getPlatform().c_str());
        resourceInfo.setVersion("1.2.3-zzz");
        ASSERT_STREQ("1.2.3-zzz", resourceInfo.getVersion().c_str());
    }
}
