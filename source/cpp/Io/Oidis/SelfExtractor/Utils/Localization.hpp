/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_SELFEXTRACTOR_UTILS_LOCALIZATION_HPP_
#define IO_OIDIS_SELFEXTRACTOR_UTILS_LOCALIZATION_HPP_

namespace Io::Oidis::SelfExtractor::Utils {
    /**
     * This singleton class holds localization table created from JSON data and provides API to get localized string.
     */
    class Localization {
     public:
        /**
         * @return Returns singleton instance.
         */
        static Localization &getInstance();

        /**
         * @return Returns localization table.
         */
        const json &getLocData() const;

        /**
         * @param $locData Specify localization table.
         */
        void setLocData(const json &$locData);

        /**
         * Translate specified key to its localized string.
         * @param $key Specify key to be localized.
         * @return Returns localized string.
         */
        string getLocalized(const string &$key);

     private:
        json locData = {};
    };
}

#endif  // IO_OIDIS_SELFEXTRACTOR_UTILS_LOCALIZATION_HPP_
